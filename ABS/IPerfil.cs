﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ABS
{
    public interface IPerfil : IEntity
    {
        string Nombre { get; set; }
        void AgregarPerfil(IPerfil p);
        void QuitarPerfil(IPerfil p);
        IList<IPerfil> ObtenerHijos();
    }
}
