﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.IO;
using Microsoft.SqlServer.Server;


namespace DAL
{
    public class Acceso
    {
        private SqlConnection Conexion = new SqlConnection(@"Data Source=.\SQLEXPRESS;Initial Catalog=PixelWaveDB;Integrated Security=True");
        private string CadenaC = @"Data Source=.\SQLEXPRESS;Initial Catalog=PixelWaveDB;Integrated Security=True";
        private SqlTransaction trans2;
        private SqlCommand cmd2;
        SqlTransaction Trx;

        void Abrir()
        {
            Conexion.Open();
        }

        void Cerrar()
        {
            Conexion.Close();
        }

        //leo un escalar
        public bool LeerScalar(string consulta)
        {
            Conexion.Open();
            SqlCommand cmd = new SqlCommand(consulta, Conexion);
            cmd.CommandType = CommandType.Text;
            try
            {
                int Respuesta = Convert.ToInt32(cmd.ExecuteScalar());

                if (Respuesta > 0) { return true; }
                else { return false; }
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            { //cierro la Conexion
                Conexion.Close();
            }
        }

        public bool Escribir(string Consulta_SQL)
        {


            Conexion.Open();

            SqlTransaction myTrans;

            SqlCommand cmd = new SqlCommand();


            cmd.CommandType = CommandType.Text;
            cmd.Connection = Conexion;
            cmd.CommandText = Consulta_SQL;
            myTrans = Conexion.BeginTransaction();

            try
            {
                cmd.Transaction = myTrans;

                cmd.ExecuteNonQuery();

                myTrans.Commit();

                return true;
            }
            catch (SqlException ex)
            {
                myTrans.Rollback();

                throw ex;
            }

            finally
            { Conexion.Close(); }



        }

        public DataSet Leer(string Procedimiento, Hashtable Parametros)
        {
            DataSet Datos = new DataSet();

            try
            {

                SqlCommand oComando = new SqlCommand();

                if (Parametros != null)
                {
                    //recorrer el hastable
                    foreach (string clave in Parametros.Keys)
                    {
                        oComando.Parameters.AddWithValue(clave, Parametros[clave]);
                    }
                }


                Abrir();

                oComando.Connection = Conexion;

                oComando.CommandType = CommandType.StoredProcedure;

                oComando.CommandText = Procedimiento;

                SqlDataAdapter Adaptador = new SqlDataAdapter(oComando);

                Adaptador.Fill(Datos);

            }
            catch (SqlException ExSql)
            {
                throw new Exception(ExSql.Message);

            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
            finally
            {
                Cerrar();
            }


            return Datos;
        }

        public DataTable Leer(string consulta)
        {
            DataTable tabla = new DataTable();
            try
            {
                SqlDataAdapter Da = new SqlDataAdapter(consulta, Conexion);
                Da.Fill(tabla);

            }
            catch (SqlException ex)
            { throw ex; }
            catch (Exception ex)
            { throw ex; }
            finally
            { //cierro la Conexion
                Conexion.Close();
            }
            return tabla;
        }

        public bool Escribir(string consulta, Hashtable Hdatos)
        {
            if (Conexion.State == ConnectionState.Closed)
            {
                Conexion.ConnectionString = CadenaC;
                Conexion.Open();
            }

            try
            {
                trans2 = Conexion.BeginTransaction();
                cmd2 = new SqlCommand(consulta, Conexion, trans2);
                cmd2.CommandType = CommandType.StoredProcedure;

                if ((Hdatos != null))
                {
                    foreach (string dato in Hdatos.Keys)
                    {
                        cmd2.Parameters.AddWithValue(dato, Hdatos[dato]);
                    }
                }

                int respuesta = cmd2.ExecuteNonQuery();
                trans2.Commit();
                return true;

            }

            catch (SqlException ex)
            {
                trans2.Rollback();

                return false;
                throw ex;
            }
            catch (Exception ex)
            {
                trans2.Rollback();
                return false;
                throw ex;
            }
            finally
            {
                Conexion.Close();
            }
        }


        public int Escribir2(string Procedimiento, Hashtable Parametros)
        {
            int CantidadFilas = 0;


            try
            {
                Abrir();

                SqlCommand oComando = new SqlCommand();

                oComando.CommandType = CommandType.StoredProcedure;
                oComando.CommandText = Procedimiento;
                oComando.Connection = Conexion;

                Trx = Conexion.BeginTransaction(IsolationLevel.Serializable);
                oComando.Transaction = Trx;

                if (Parametros != null)
                {
                    foreach (string llave in Parametros.Keys)
                    {
                        oComando.Parameters.AddWithValue(llave, Parametros[llave]);
                    }
                }

                CantidadFilas = oComando.ExecuteNonQuery();
                Trx.Commit();

            }

            catch (SqlException ExSql)
            {
                Trx.Rollback();
                throw new Exception(ExSql.Message);

            }
            catch (Exception Ex)
            {
                Trx.Rollback();
                throw new Exception(Ex.Message);
            }
            finally
            {
                Cerrar();
            }

            return CantidadFilas;

        }

        public string NombreDeLaBase()
        {
            return Conexion.Database.ToString();
        }

        public DataTable LeerDataTable(string Consulta, Hashtable Hdatos)
        {
            DataTable Dt = new DataTable();
            SqlDataAdapter Da;
            cmd2 = new SqlCommand(Consulta, Conexion);
            cmd2.CommandType = CommandType.StoredProcedure;

            try
            {
                Da = new SqlDataAdapter(cmd2);

                if ((Hdatos != null))
                {
                    foreach (string dato in Hdatos.Keys)
                    {
                        cmd2.Parameters.AddWithValue(dato, Hdatos[dato]);
                    }
                }
            }

            catch (SqlException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            Da.Fill(Dt);
            return Dt;

        }


    }
}