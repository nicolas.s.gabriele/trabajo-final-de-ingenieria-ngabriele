﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPP;
using BE;

namespace BLL
{
    public class BLLFacturacion
    {
        MPPFacturacion oMPPFacturacion;
        public BLLFacturacion()
        {
            oMPPFacturacion = new MPPFacturacion();
        }
        public bool Guardar(BEFacturaCabecera oBEFacCab, BEFacturaDetalle oBEFacDet, BEPagos oBEPagos)
        {
            return oMPPFacturacion.Guardar(oBEFacCab, oBEFacDet, oBEPagos);
        }
        public List<BEFacturaDetalle> listarFacturasPorCliente(string usuario)
        {
            return oMPPFacturacion.listarFacturasPorCliente(usuario);
        }
    }
}
