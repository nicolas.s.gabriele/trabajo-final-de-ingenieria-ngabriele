﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPP;
using BE;

namespace BLL
{
    public class BLLPedido
    {
        MPPPedido oMPPPedido;
        public BLLPedido()
        {
            oMPPPedido = new MPPPedido();
        }
        public bool Guardar(BEPedidos oBEPed)
        {
            return oMPPPedido.Guardar(oBEPed);
        }

        public List<BEPedidos> ListarPedidos()
        {
            return oMPPPedido.ListarPedidos();
        }

        public bool actualizarPedido(int IdPedido)
        {
            return oMPPPedido.modificarEstadoPedido(IdPedido);
        }
    }
}
