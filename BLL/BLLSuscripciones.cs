﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using MPP;

namespace BLL
{
    public class BLLSuscripciones
    {
        MPPSuscripciones oMPPSuscripciones;

        public BLLSuscripciones()
        {
            oMPPSuscripciones = new MPPSuscripciones();
        }

        public List<BESuscripciones> ListarSuscripcionCliente(int Cliente)
        {
            return oMPPSuscripciones.ListarSuscripcionCliente(Cliente);
        }

        public bool GuardarSuscripcion(BESuscripciones oBESuscripciones)
        {
            return oMPPSuscripciones.GuardarSuscripcion(oBESuscripciones);
        }

        public List<BESuscripciones> ListarSuscripciones()
        {
            return oMPPSuscripciones.ListarSuscripciones();
        }
    }
}
