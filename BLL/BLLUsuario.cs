﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPP;
using BE;
using SERVICIOS;

namespace BLL
{
    public class BLLUsuario
    {
        public BEUsuario Logear(int id, string Username, string Password)
        {
            BEUsuario _Usuario = new BEUsuario();
            BEUsuario _Usuario2 = new BEUsuario();
            BLLUsuario _Login = new BLLUsuario();

            _Usuario.Email = Username;
            _Usuario.Contraseña = Password;
            _Usuario2 = _Login.ListarUnUser(_Usuario);

            return _Usuario2;
        }

        public BECliente LogearCliente(int id, string Username, string Password)
        {
            BECliente _Usuario = new BECliente();
            BECliente _Usuario2 = new BECliente();
            BLLUsuario _Login = new BLLUsuario();

            _Usuario.Email = Username;
            _Usuario.Contraseña = Password;
            _Usuario2 = _Login.ListarUnCliente(_Usuario);

            return _Usuario2;
        }

        public BECliente ListarUnCliente(BECliente oBECliente)
        {
            BECliente User = new BECliente();
            MPPCliente MPP_Cli = new MPPCliente();
            return User = MPP_Cli.ListarUn_Cli(oBECliente);
        }

        public BEUsuario ListarUnUser(BEUsuario oBEUsuario)
        {
            BEUsuario User = new BEUsuario();
            MPPUsuario MPP_User = new MPPUsuario();
            return User = MPP_User.ListarUn_User(oBEUsuario);
        }


        public bool Registrar(BEUsuario oBEUsuario)
        {//instancio un objeto de la clase datos para operar con la BD
            MPPUsuario MPP_User = new MPPUsuario();
            return MPP_User.Registrar(oBEUsuario);

        }

        public List<BEUsuario> LeerUsuarios()
        {
            List<BEUsuario> Datos = new List<BEUsuario>();
            MPPUsuario MPP_User = new MPPUsuario();

            Datos = MPP_User.LeerUsuarios();

            return Datos;
        }

        public int InsertarUsuario(BEUsuario oUsuario)
        {
            MPPUsuario oMapp = new MPPUsuario();
            int filas = oMapp.InsertarUsuario(oUsuario);
            return filas;
        }

        public bool Modificar(BEUsuario oBEUsu)
        {
            MPPUsuario oMapp = new MPPUsuario();
            return oMapp.Modificar(oBEUsu);
        }

        public int EditarUsuario(BEUsuario oUsuario)
        {
            MPPUsuario oMapp = new MPPUsuario();

            oUsuario.Contraseña = ServicioEncriptado.Encriptar(oUsuario.Contraseña);

            int filas = oMapp.EditarUsuario(oUsuario);

            return filas;
        }

        public int EliminarUsuario(BEUsuario oUsuario)
        {
            MPPUsuario oMapp = new MPPUsuario();

            int filas = oMapp.EliminarUsuario(oUsuario);

            return filas;
        }
        public bool BajaUsuario(BEUsuario oBEUsuario)
        {
            MPPUsuario oMapp = new MPPUsuario();
            return oMapp.Baja(oBEUsuario);
        }

        public List<BEUsuario> ListarUsuarios()
        {
            MPPUsuario oMapp = new MPPUsuario();
            return oMapp.ListarUsuarios();
        }
    }
}
