﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPP;
using BE;

namespace BLL
{
    public class BLLProveedores
    {
        MPPProveedores oMPPProveedores;

        public BLLProveedores()
        {
            oMPPProveedores = new MPPProveedores();
        }

        public List<BEProveedores> ListarProveedores()
        {
            return oMPPProveedores.ListarProveedores();
        }

    }
}
