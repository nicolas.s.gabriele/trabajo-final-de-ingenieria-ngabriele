﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using MPP;
using BLL;

namespace BLL
{
    public class BLLOpinion
    {
        public BLLOpinion()
        {
            oMPPOpinion = new MPPOpinion();
        }
        MPPOpinion oMPPOpinion;
        public bool Guardar(BEOpinion oBEOpinion)
        {
            return oMPPOpinion.Guardar(oBEOpinion);
        }
    }
}
