﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPP;
using BE;

namespace BLL
{
    public class BLLMediosDePago
    {
        public BLLMediosDePago()
        {
            oMPPMediosDePago = new MPPMediosDePago();
        }

        MPPMediosDePago oMPPMediosDePago;
        public List<BEMediosDePago> ListarMediosDePago()
        {
            return oMPPMediosDePago.ListarMediosDePago();
        }
    }
}
