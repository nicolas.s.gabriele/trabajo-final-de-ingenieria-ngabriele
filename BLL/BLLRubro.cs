﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using MPP;

namespace BLL
{
    public class BLLRubro
    {
        public BLLRubro()
        {
            oMPPRubro = new MPPRubro();
        }

        MPPRubro oMPPRubro;
        public List<BERubro> ListarRubros()
        {
            return oMPPRubro.ListarRubros();
        }
    }
}
