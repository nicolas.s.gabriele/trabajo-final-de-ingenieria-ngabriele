﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using MPP;

namespace BLL
{
    public class BLLOCompra
    {
        MPPOcompra oMPPCompra;
        public BLLOCompra()
        {
            oMPPCompra = new MPPOcompra();
        }
        public bool Guardar(BEOCompra oBEComp)
        {
            return oMPPCompra.Guardar(oBEComp);
        }
    }
}
