﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using MPP;

namespace BLL
{
    public class BLLInsumos
    {
        MPPInsumos oMPPInsumos;

        public BLLInsumos()
        {
            oMPPInsumos = new MPPInsumos();
        }

        public List<BEInsumos> ListarInsumos()
        {
            return oMPPInsumos.ListarInsumos();
        }

        public List<BEInsumos> ListarPrecioPorInsumo(string Insumo)
        {
            return oMPPInsumos.ListarPrecioPorInsumo(Insumo);
        }
    }
}
