﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using MPP;

namespace BLL
{
    public class BLLServicio
    {
        MPPServicio oMPPServicio;
        public BLLServicio()
        {
            oMPPServicio = new MPPServicio();
        }
        public bool Baja(BEServicio oBEServ)
        {
            return oMPPServicio.Baja(oBEServ);
        }

        public bool Guardar(BEServicio oBEServ)
        {
            return oMPPServicio.Guardar(oBEServ);
        }

        public List<BEServicio> ListarTodo()
        {
            return oMPPServicio.ListarTodo();
        }

        public List<BEServicio> ListarPrecioPorServicio(string Servicio)
        {
            return oMPPServicio.ListarPrecioPorServicio(Servicio);
        }

        public List<BEPedidos> ServiciosContratadosPorMes(int mes, int año)
        {
            MPPServicio oMPPServicios = new MPPServicio();
            return oMPPServicios.ServiciosContratadosPorMes(mes, año);
        }
    }
}
