﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MPP;
using BE;

namespace BLL
{
    public class BLLCliente
    {
        MPPCliente oMPPCliente;

        public BLLCliente()
        {
            oMPPCliente = new MPPCliente();
        }

        public bool Baja(BECliente oBECliente)
        {
            return oMPPCliente.Baja(oBECliente);
        }

        public bool Guardar(BECliente oBECli)
        {
            return oMPPCliente.Guardar(oBECli);
        }

        public bool Modificar(BECliente oBECli)
        {
            return oMPPCliente.Modificar(oBECli);
        }

        public List<BECliente> ListarClientes()
        {
            return oMPPCliente.ListarClientes();
        }

        public List<BECliente> ListarCodigoCliente(string Email)
        {
            return oMPPCliente.ListarCodigoCliente(Email);
        }
    }
}
