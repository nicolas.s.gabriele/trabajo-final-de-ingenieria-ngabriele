﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Data;
using System.Collections;
using BE;

namespace MPP
{
    public class MPPUsuario
    {
        public bool Registrar(BEUsuario oBEUsuario)
        {//instancio un objeto de la clase datos para operar con la BD
            Acceso oDatos = new Acceso();
            string Consulta_SQL = "Insert into Usuario (Email,Contraseña,Subscripcion) values('" + oBEUsuario.Email + "', '" + oBEUsuario.Contraseña + "', '1') ";
            return oDatos.Escribir(Consulta_SQL);
        }

        public BEUsuario ListarUn_User(BEUsuario oBEUsuario)
        {
            Acceso dal = new Acceso();
            DataSet ds = new DataSet();
            Hashtable hs = new Hashtable();
            BEUsuario Usuario = default(BEUsuario);
            int Contador = 0;
            DateTime FechaIngreso = new DateTime();

            //hs.Add("@IDUsuario", oBEUsuario.ID);
            hs.Add("@Email", oBEUsuario.Email);
            hs.Add("@Contraseña", oBEUsuario.Contraseña);

            ds = dal.Leer("ListarUsuario", hs);

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    Usuario = new BEUsuario();
                    Usuario.ID = Convert.ToInt32(item["IDUsuario"]);
                    Usuario.Email = item["Email"].ToString();
                    Usuario.Contraseña = item["Contraseña"].ToString();
                    oBEUsuario.ID = Usuario.ID;
                    //   Contador = Convert.ToInt32(item["Contador"]);
                    //FechaIngreso = Convert.ToDateTime(item["FechaIngreso"]);
                    Usuario.Perfil = Convert.ToString(item["id_perfil"]);
                }

                return Usuario;

                //usuario bloqueado, contador =3

                // me fijo si ya pasaron los 5 minutos
                TimeSpan tiempo = DateTime.Now.Subtract(FechaIngreso);

                //if (Contador >= 3 && tiempo.Minutes < 5)
                //{
                //    return null;
                //}

                //if (Contador >= 3 && tiempo.Minutes > 5)
                //{
                //    Hashtable Parametros2 = new Hashtable();
                //    Parametros2.Add("Contador", Contador);
                //    Parametros2.Add("FechaIngreso", DateTime.Now);
                //    Parametros2.Add("Usuario", oBEUsuario.Email);

                //    //reseteo el contador
                //    dal.Escribir("ResetContador", Parametros2);

                //    oBEUsuario.ID = Usuario.ID;
                //    return Usuario;
                //}

                //if (Contador < 3)
                //{
                //    oBEUsuario.ID = Usuario.ID;
                //    return Usuario;
                //}
                //else
                //{
                //    return null;
                //}

            }
            else
            {
                return null;
            }
        }

        public List<BEUsuario> LeerUsuarios()
        {
            Acceso Dal = new Acceso();
            DataSet Datos = Dal.Leer("LeerUsuarios", null);

            List<BEUsuario> Lista = new List<BEUsuario>();

            if (Datos.Tables.Count > 0)
            {

                foreach (DataRow fila in Datos.Tables[0].Rows)
                {
                    BEUsuario oUsuario = new BEUsuario();

                    oUsuario.Nombre = fila["Nombre"].ToString();
                    oUsuario.Apellido = fila["Apellido"].ToString();
                    oUsuario.Email = fila["Email"].ToString();
                    oUsuario.ID = Convert.ToInt32(fila["IDUsuario"].ToString());
                    oUsuario.Contraseña = fila["Contraseña"].ToString();
                    oUsuario.Contador = Convert.ToInt32(fila["Contador"].ToString());
                    if (fila["FechaIngreso"] != DBNull.Value)
                    {
                        oUsuario.FechaIngreso = Convert.ToDateTime(fila["FechaIngreso"].ToString());
                    }

                    oUsuario.Perfil = Convert.ToString(fila["perfil"]);


                    Lista.Add(oUsuario);
                }

            }
            return Lista;
        }

        public int InsertarUsuario(BEUsuario oUsuario)
        {
            Acceso _Acceso = new Acceso();

            Hashtable Parametros = new Hashtable();

            Parametros.Add("email", oUsuario.Email);
            Parametros.Add("contraseña", oUsuario.Contraseña);
            Parametros.Add("nombre", oUsuario.Nombre);
            Parametros.Add("apellido", oUsuario.Apellido);
            Parametros.Add("fechaAlta", oUsuario.FechaIngreso);
            Parametros.Add("dni", oUsuario.DNI);
            //Parametros.Add("id_perfil", 2);

            int filas = _Acceso.Escribir2("sp_altaUsuario", Parametros);

            return filas;
        }

        public int EditarUsuario(BEUsuario oUsuario)
        {
            Acceso _Acceso = new Acceso();

            Hashtable Parametros = new Hashtable();

            string[] subEmail = oUsuario.Email.Split('|');

            string EmailViejo = subEmail[0];
            string EmailNuevo = subEmail[1];

            Parametros.Add("EmailViejo", EmailViejo);
            Parametros.Add("Email", EmailNuevo);
            Parametros.Add("Nombre", oUsuario.Nombre);
            Parametros.Add("Apellido", oUsuario.Apellido);
            Parametros.Add("Password", oUsuario.Contraseña);
            Parametros.Add("Contador", oUsuario.Contador);

            int filas = _Acceso.Escribir2("EditarUsuario", Parametros);

            return filas;
        }

        public int EliminarUsuario(BEUsuario oUsuario)
        {
            Acceso _Acceso = new Acceso();

            Hashtable Parametros = new Hashtable();

            Parametros.Add("Email", oUsuario.Email);

            int filas = _Acceso.Escribir2("EliminarUsuario", Parametros);

            return filas;
        }

        Acceso oDatos = new Acceso();
        Hashtable Hdatos;

        public List<BEUsuario> ListarUsuarios()
        {
            List<BEUsuario> ListarUsuarios = new List<BEUsuario>();

            string Consulta = "sp_listarUsuarios";

            DataTable Tabla = oDatos.Leer(Consulta);

            if (Tabla.Rows.Count > 0)
            {
                foreach (DataRow fila in Tabla.Rows)
                {
                    BEUsuario oBEUsu = new BEUsuario();
                    oBEUsu.ID = Convert.ToInt32(fila["IDUsuario"]);
                    oBEUsu.DNI = Convert.ToString(fila["DNI"]);
                    oBEUsu.Apellido = Convert.ToString(fila["Apellido"]);
                    oBEUsu.Nombre = Convert.ToString(fila["Nombre"]);
                    oBEUsu.Email = Convert.ToString(fila["Email"]);
                    oBEUsu.Contraseña = Convert.ToString(fila["Contraseña"]);
                    ListarUsuarios.Add(oBEUsu);
                }
            }
            return ListarUsuarios;
        }

        public bool Baja(BEUsuario oBEUsu)
        {
            string Consulta_SQL = "sp_bajaUsuario";
            oDatos = new Acceso();
            Hdatos = new Hashtable();
            Hdatos.Add("@codUsuario", oBEUsu.ID);
            return oDatos.Escribir(Consulta_SQL, Hdatos);
        }

        public bool Modificar(BEUsuario oBEUsu)
        {
            Acceso oDatos;
            string Consulta_SQL;
            bool valor = false;

            oDatos = new Acceso();
            Consulta_SQL = "sp_modificarCliente";
            Hdatos = new Hashtable();
            Hdatos.Add("@Id", oBEUsu.ID);
            Hdatos.Add("@nombre", oBEUsu.Nombre);
            Hdatos.Add("@Apellido", oBEUsu.Apellido);
            Hdatos.Add("@DNI", oBEUsu.DNI);
            Hdatos.Add("@Email", oBEUsu.Email);
            return oDatos.Escribir(Consulta_SQL, Hdatos);

        }
    }
}
