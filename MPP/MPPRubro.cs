﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.IO;
using System.Collections;
using BE;
using DAL;

namespace MPP
{
    public class MPPRubro
    {
        Acceso oDatos = new Acceso();
        Hashtable Hdatos;

        public List<BERubro> ListarRubros()
        {
            List<BERubro> ListarRubros = new List<BERubro>();

            string Consulta = "sp_listarRubros";

            DataTable Tabla = oDatos.Leer(Consulta);

            if (Tabla.Rows.Count > 0)
            {
                foreach (DataRow fila in Tabla.Rows)
                {
                    BERubro oBEMP = new BERubro();
                    oBEMP.Id = Convert.ToInt32(fila["Id"]);
                    oBEMP.Descripcion = Convert.ToString(fila["Rubros"]);
                    ListarRubros.Add(oBEMP);
                }
            }
            return ListarRubros;
        }
    }
}
