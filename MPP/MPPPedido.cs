﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using BE;
using DAL;
using System.Data;

namespace MPP
{
    public class MPPPedido
    {
        Acceso oDatos;
        string Consulta_SQL;
        bool valor = false;
        Hashtable Hdatos;
        public bool Guardar(BEPedidos oBEPed)
        {
            Acceso oDatos;
            string Consulta_SQL;
            bool valor = false;
            Hashtable Hdatos;

            //if (oBEProd.Codigo != 0)
            //{
            //    oDatos = new Acceso();
            //    Consulta_SQL = "sp_modificarProducto";
            //    Hdatos = new Hashtable();
            //    Hdatos.Add("@codProducto", oBEProd.Codigo);
            //    Hdatos.Add("@TipoPrenda", oBEProd.Tipo_De_Prenda);
            //    Hdatos.Add("@Modelo", oBEProd.Modelo);
            //    Hdatos.Add("@Color", oBEProd.Color);
            //    Hdatos.Add("@Talle", oBEProd.Talle);
            //    Hdatos.Add("@Precio", oBEProd.Precio);
            //    return oDatos.Escribir(Consulta_SQL, Hdatos);
            //}
            //else
            //{
                oDatos = new Acceso();
                Consulta_SQL = "sp_altaPedido";
                Hdatos = new Hashtable();
                Hdatos.Add("@Servicio", oBEPed.servicio);
                Hdatos.Add("@MedioDePago", oBEPed.medioDePago);
                Hdatos.Add("@RubroEmpresa", oBEPed.rubroEmpresa);
                Hdatos.Add("@Observaciones", oBEPed.observaciones);
                Hdatos.Add("@precio", oBEPed.precio);
                Hdatos.Add("@Fecha", oBEPed.Fecha);
                Hdatos.Add("@usuario", oBEPed.Usuario);

                return oDatos.Escribir(Consulta_SQL, Hdatos);
            //}

            //return false;
        }

        public List<BEPedidos> ListarPedidos()
        {
            Acceso oDatos;
            oDatos = new Acceso();

            List<BEPedidos> ListarPedidos = new List<BEPedidos>();

            string Consulta = "sp_ListarPedido";

            DataTable Tabla = oDatos.Leer(Consulta);

            if (Tabla.Rows.Count > 0)
            {
                foreach (DataRow fila in Tabla.Rows)
                {
                    BEPedidos oBEPed = new BEPedidos();
                    oBEPed.Id = Convert.ToInt32(fila["Id"]);
                    oBEPed.servicio = Convert.ToString(fila["Servicio"]);
                    oBEPed.precio = Convert.ToInt64(fila["Precio"]);
                    oBEPed.Fecha = Convert.ToDateTime(fila["Fecha"]);
                    oBEPed.Usuario = Convert.ToString(fila["Usuario"]);

                    ListarPedidos.Add(oBEPed);
                }
            }
            return ListarPedidos;
        }

        public bool modificarEstadoPedido(int IdPedido)
        {
            string Consulta_SQL = "sp_modificarEstadoPedido";
            oDatos = new Acceso();
            Hdatos = new Hashtable();
            Hdatos.Add("@IdPedido", IdPedido);
            return oDatos.Escribir(Consulta_SQL, Hdatos);
        }
    }
}
