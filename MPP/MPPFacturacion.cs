﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Collections;
using BE;
using DAL;


namespace MPP
{
    public class MPPFacturacion
    {

        Acceso oDatos = new Acceso();
        Hashtable Hdatos;

        BEFacturaCabecera oBEFacCab = new BEFacturaCabecera();
        BEFacturaDetalle oBEFacDet = new BEFacturaDetalle();
        BEPedidos oBEPed = new BEPedidos();
        BECliente oBECLi = new BECliente();
        public bool Guardar(BEFacturaCabecera oBEFacCab, BEFacturaDetalle oBEFacDet, BEPagos oBEPagos)
        {
            Acceso oDatos;
            string Consulta_SQL;
            bool valor = false;

            oDatos = new Acceso();
            Consulta_SQL = "sp_altaFactura";
            Hdatos = new Hashtable();
            Hdatos.Add("@IdPedido", oBEFacDet.IdPedido);
            Hdatos.Add("@servicio", oBEFacDet.Detalle);
            Hdatos.Add("@precioTotal", oBEFacDet.PrecioTotal);
            Hdatos.Add("@TipoTarjeta", oBEPagos.TipoTarjeta);
            Hdatos.Add("@NumeroTarjeta", oBEPagos.NumeroTarjeta);
            Hdatos.Add("@CodigoSeguridad", oBEPagos.CodigoSeguridad);
            Hdatos.Add("@Cuotas", oBEPagos.Cuotas);

            return oDatos.Escribir(Consulta_SQL, Hdatos);
        }

        public List<BEFacturaDetalle> listarFacturasPorCliente(string usuario)
        {
            List<BEFacturaDetalle> listarFacturasPorCliente = new List<BEFacturaDetalle>();

            string Consulta = "sp_listarFacturas";
            Hashtable Hdatos = new Hashtable();
            
            Hdatos.Add("@cliente", usuario);

            DataTable Tabla = oDatos.LeerDataTable(Consulta,Hdatos);

            if (Tabla.Rows.Count > 0)
            {
                foreach (DataRow fila in Tabla.Rows)
                {
                    oBEFacDet.IdCabecera = Convert.ToInt32(fila["IdCabecera"]);
                    oBEFacDet.Detalle = Convert.ToString(fila["Detalle"]);
                    oBEFacDet.PrecioTotal = Convert.ToInt32(fila["PrecioTotal"]);
                    oBEFacDet.IdPedido = Convert.ToInt32(fila["IdPedido"]);
                    oBEFacDet.Email = usuario;
                    oBEFacDet.Estado = Convert.ToString(fila["Estado"]);
                    listarFacturasPorCliente.Add(oBEFacDet);
                }
            }
            return listarFacturasPorCliente;
        }
    }
}
