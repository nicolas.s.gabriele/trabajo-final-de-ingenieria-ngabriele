﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using DAL;
using BE;

namespace MPP
{
    public class MPPCliente
    {
        Acceso oDatos = new Acceso();
        Hashtable Hdatos;
        public bool Guardar(BECliente oBECli)
        {
            Acceso oDatos;
            string Consulta_SQL;
            bool valor = false;

            oDatos = new Acceso();
            Consulta_SQL = "sp_altaCliente";
            Hdatos = new Hashtable();
            Hdatos.Add("@nombre", oBECli.Nombre);
            Hdatos.Add("@apellido", oBECli.Apellido);
            Hdatos.Add("@dni", oBECli.Dni);
            Hdatos.Add("@celular", oBECli.Celular);
            Hdatos.Add("@email", oBECli.Email);
            Hdatos.Add("@contraseña", oBECli.Contraseña);

            return oDatos.Escribir(Consulta_SQL, Hdatos);
        }

        public bool Modificar(BECliente oBECli)
        {
            Acceso oDatos;
            string Consulta_SQL;
            bool valor = false;

            oDatos = new Acceso();
            Consulta_SQL = "sp_modificarCliente";
            Hdatos = new Hashtable();
            Hdatos.Add("@nombre", oBECli.Nombre);
            Hdatos.Add("@apellido", oBECli.Apellido);
            Hdatos.Add("@dni", oBECli.Dni);
            Hdatos.Add("@celular", oBECli.Celular);
            Hdatos.Add("@email", oBECli.Email);
            Hdatos.Add("@contraseña", oBECli.Contraseña);
            return oDatos.Escribir(Consulta_SQL, Hdatos);
            
        }

        public bool Baja(BECliente oBECli)
        {
            string Consulta_SQL = "sp_bajaCliente";
            oDatos = new Acceso();
            Hdatos = new Hashtable();
            Hdatos.Add("@codCliente", oBECli.Dni);
            return oDatos.Escribir(Consulta_SQL, Hdatos);
        }

        public List<BECliente> ListarClientes()
        {
            List<BECliente> ListarClientes = new List<BECliente>();

            string Consulta = "sp_listarCliente";

            DataTable Tabla = oDatos.Leer(Consulta);

            if (Tabla.Rows.Count > 0)
            {
                foreach (DataRow fila in Tabla.Rows)
                {
                    BECliente oBECLi = new BECliente();
                    oBECLi.Dni = Convert.ToString(fila["DNI"]);
                    oBECLi.Apellido = Convert.ToString(fila["Apellido"]);
                    oBECLi.Nombre = Convert.ToString(fila["Nombre"]);
                    oBECLi.Email = Convert.ToString(fila["Email"]);
                    oBECLi.Celular = Convert.ToString(fila["Celular"]);
                    oBECLi.Contraseña = Convert.ToString(fila["Contraseña"]);
                    ListarClientes.Add(oBECLi);
                }
            }
            return ListarClientes;
        }

        public List<BECliente> ListarCodigoCliente(string Email)
        {
            List<BECliente> listaCliente = new List<BECliente>();

            string Consulta = "sp_obtenerUsuarioCliente";
            Hashtable hdatos = new Hashtable();
            hdatos.Add("@Email", Email);

            DataTable Tabla = oDatos.LeerDataTable(Consulta, hdatos);

            if (Tabla.Rows.Count > 0)
            {
                foreach (DataRow fila in Tabla.Rows)
                {
                    BECliente oBECliente = new BECliente();
                    oBECliente.Id = Convert.ToInt32(fila["Id"]);
                    listaCliente.Add(oBECliente);
                }
            }
            return listaCliente;
        }

        public BECliente ListarUn_Cli(BECliente oBECliente)
        {
            Acceso dal = new Acceso();
            DataSet ds = new DataSet();
            Hashtable hs = new Hashtable();
            BECliente Usuario = default(BECliente);
            int Contador = 0;
            DateTime FechaIngreso = new DateTime();

            //hs.Add("@IDUsuario", oBEUsuario.ID);
            hs.Add("@Email", oBECliente.Email);
            hs.Add("@Contraseña", oBECliente.Contraseña);

            ds = dal.Leer("ListarCliente", hs);

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    Usuario = new BECliente();
                    Usuario.Id = Convert.ToInt32(item["Id"]);
                    Usuario.Email = item["Email"].ToString();
                    Usuario.Contraseña = item["Contraseña"].ToString();
                    oBECliente.Id = Usuario.Id;
                    //   Contador = Convert.ToInt32(item["Contador"]);
                    //FechaIngreso = Convert.ToDateTime(item["FechaIngreso"]);
                    Usuario.Perfil = Convert.ToString(item["id_perfil"]);
                }

                return Usuario;

                //usuario bloqueado, contador =3

                // me fijo si ya pasaron los 5 minutos
                TimeSpan tiempo = DateTime.Now.Subtract(FechaIngreso);

                //if (Contador >= 3 && tiempo.Minutes < 5)
                //{
                //    return null;
                //}

                //if (Contador >= 3 && tiempo.Minutes > 5)
                //{
                //    Hashtable Parametros2 = new Hashtable();
                //    Parametros2.Add("Contador", Contador);
                //    Parametros2.Add("FechaIngreso", DateTime.Now);
                //    Parametros2.Add("Usuario", oBEUsuario.Email);

                //    //reseteo el contador
                //    dal.Escribir("ResetContador", Parametros2);

                //    oBEUsuario.ID = Usuario.ID;
                //    return Usuario;
                //}

                //if (Contador < 3)
                //{
                //    oBEUsuario.ID = Usuario.ID;
                //    return Usuario;
                //}
                //else
                //{
                //    return null;
                //}

            }
            else
            {
                return null;
            }
        }
    }
}
