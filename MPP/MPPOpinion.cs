﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using BE;
using DAL;

namespace MPP
{
    public class MPPOpinion
    {
        Hashtable Hdatos;

        public bool Guardar(BEOpinion oBEOpinion)
        {
            Acceso oDatos;
            string Consulta_SQL;
            bool valor = false;

            oDatos = new Acceso();
            Consulta_SQL = "sp_altaOpinion";
            Hdatos = new Hashtable();
            Hdatos.Add("@cantEstrella", oBEOpinion.Estrella);
            Hdatos.Add("@descripcion", oBEOpinion.Descripcion);
            Hdatos.Add("@cliente", oBEOpinion.Cliente);
            Hdatos.Add("@fecha", oBEOpinion.Fecha);

            return oDatos.Escribir(Consulta_SQL, Hdatos);
        }
    }
}
