﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using DAL;
using System.Collections;
using System.Data;

namespace MPP
{
    public class MPPServicio
    {
        Acceso oDatos = new Acceso();
        Hashtable Hdatos;
        //BEServicio oBEServ
        public bool Baja(BEServicio oBEServ)
        {
            string Consulta_SQL = "sp_bajaServicio";
            oDatos = new Acceso();
            Hdatos = new Hashtable();
            Hdatos.Add("@codServicio", oBEServ.Id);
            return oDatos.Escribir(Consulta_SQL, Hdatos);
        }

        public bool Guardar(BEServicio oBEServ)
        {
            Acceso oDatos;
            string Consulta_SQL;
            bool valor = false;

            if (oBEServ.Id != 0)
            {
                oDatos = new Acceso();
                Consulta_SQL = "sp_modificarServicio";
                Hdatos = new Hashtable();
                Hdatos.Add("@codigoServicio", oBEServ.Id);                
                Hdatos.Add("@nombreServicio", oBEServ.Servicio);
                Hdatos.Add("@descripcionServicio", oBEServ.Descripcion);
                Hdatos.Add("@precioServicio", oBEServ.Precio);
                return oDatos.Escribir(Consulta_SQL, Hdatos);
            }
            else
            {
                oDatos = new Acceso();
                Consulta_SQL = "sp_altaServicio";
                Hdatos = new Hashtable();
                Hdatos.Add("@Servicio", oBEServ.Servicio);
                Hdatos.Add("@Descripcion", oBEServ.Descripcion);
                Hdatos.Add("@Activo", 1);
                Hdatos.Add("@Precio", oBEServ.Precio);

                return oDatos.Escribir(Consulta_SQL, Hdatos);
            }

            return false;
        }

        public List<BEServicio> ListarTodo()
        {
            List<BEServicio> ListarServicios = new List<BEServicio>();

            string Consulta = "sp_listarServicio";

            DataTable Tabla = oDatos.Leer(Consulta);

            if (Tabla.Rows.Count > 0)
            {
                foreach (DataRow fila in Tabla.Rows)
                {
                    BEServicio oBEServ = new BEServicio();
                    oBEServ.Id = Convert.ToInt32(fila["Id"]);
                    oBEServ.Servicio  = fila["Servicio"].ToString();
                    oBEServ.Descripcion = fila["Descripcion"].ToString();
                    oBEServ.Precio = Convert.ToInt32(fila["Precio"]);
                    ListarServicios.Add(oBEServ);
                }
            }
            return ListarServicios;
        }

        public List<BEServicio> ListarPrecioPorServicio(string Servicio)
        {
            List<BEServicio> listaPrecio = new List<BEServicio>();

            string Consulta = "sp_ObtenerPrecioServicio";
            Hashtable hdatos = new Hashtable();
            hdatos.Add("@nombre", Servicio);

            DataTable Tabla = oDatos.LeerDataTable(Consulta, hdatos);

            if (Tabla.Rows.Count > 0)
            {
                foreach (DataRow fila in Tabla.Rows)
                {
                    BEServicio oBEServicio = new BEServicio();
                    oBEServicio.Precio = Convert.ToInt32(fila["Precio"]);
                    listaPrecio.Add(oBEServicio);
                }
            }
            return listaPrecio;
        }

        public List<BEPedidos> ServiciosContratadosPorMes(int mes, int año)
        {
            Acceso oDatos = new Acceso();
            List<BEPedidos> ListarServiciosMasVendidosPorMes = new List<BEPedidos>();
            string Consulta = "sp_servicosMasVendidoPorMes";
            Hashtable hdatos = new Hashtable();
            hdatos.Add("@mes", mes);
            hdatos.Add("@año", año);
            DataTable Tabla = oDatos.LeerDataTable(Consulta, hdatos);

            if (Tabla.Rows.Count > 0)
            {
                foreach (DataRow fila in Tabla.Rows)
                {
                    BEPedidos oBEPedidos = new BEPedidos();
                    oBEPedidos.servicio = fila["Servicio"].ToString();
                    oBEPedidos.cantidad = Convert.ToInt32(fila["Cantidad"]);
                    ListarServiciosMasVendidosPorMes.Add(oBEPedidos);
                }
            }
            return ListarServiciosMasVendidosPorMes;
        }
    }
}
