﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using DAL;
using System.Data;

namespace MPP
{
    public class MPPProveedores
    {
        Acceso oDatos = new Acceso();
        Hashtable Hdatos;

        public List<BEProveedores> ListarProveedores()
        {
            List<BEProveedores> ListarProveedores = new List<BEProveedores>();

            string Consulta = "sp_listarProveedores";

            DataTable Tabla = oDatos.Leer(Consulta);

            if (Tabla.Rows.Count > 0)
            {
                foreach (DataRow fila in Tabla.Rows)
                {
                    BEProveedores oBEProv = new BEProveedores();
                    oBEProv.Id = Convert.ToInt32(fila["Id"]);
                    oBEProv.RazonSocial = Convert.ToString(fila["RazonSocial"]);
                    ListarProveedores.Add(oBEProv);
                }
            }
            return ListarProveedores;
        }
    }
}
