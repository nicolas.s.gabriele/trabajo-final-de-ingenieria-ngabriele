﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using DAL;


namespace MPP
{
    public class MPPOcompra
    {
        public bool Guardar(BEOCompra oBEOComp)
        {
            Acceso oDatos;
            string Consulta_SQL;
            bool valor = false;
            Hashtable Hdatos;

            oDatos = new Acceso();
            Consulta_SQL = "sp_altaOCompra";
            Hdatos = new Hashtable();
            Hdatos.Add("@Proveedor", oBEOComp.Proveedor);
            Hdatos.Add("@Insumo", oBEOComp.Insumo);
            Hdatos.Add("@Cantidad", oBEOComp.Cantidad);
            Hdatos.Add("@Precio", oBEOComp.Precio);
            return oDatos.Escribir(Consulta_SQL, Hdatos);
        }
    }
}
