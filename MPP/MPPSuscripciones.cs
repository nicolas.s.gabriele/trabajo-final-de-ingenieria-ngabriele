﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using System.Collections;
using System.Data;
using DAL;

namespace MPP
{

    public class MPPSuscripciones
    {
        Acceso oDatos = new Acceso();
        Hashtable Hdatos;

        public List<BESuscripciones> ListarSuscripcionCliente(int Cliente)
        {
            List<BESuscripciones> listaSuscripcion = new List<BESuscripciones>();

            string Consulta = "sp_SuscripcionPorCliente";
            Hashtable hdatos = new Hashtable();
            hdatos.Add("@cliente", Cliente);

            DataTable Tabla = oDatos.LeerDataTable(Consulta, hdatos);

            if (Tabla.Rows.Count > 0)
            {
                foreach (DataRow fila in Tabla.Rows)
                {
                    BESuscripciones oBESuscripcion = new BESuscripciones();
                    oBESuscripcion.FechaVencimiento = Convert.ToDateTime(fila["FechaVencimiento"]);
                    oBESuscripcion.InicioSuscripcion = Convert.ToDateTime(fila["InicioSuscripcion"]);
                    oBESuscripcion.PlanSuscripcion = Convert.ToString(fila["PlanSuscripcion"]);
                    oBESuscripcion.TipoPlan = Convert.ToString(fila["Servicio"]);
                    listaSuscripcion.Add(oBESuscripcion);
                }
            }
            return listaSuscripcion;
        }

        public bool GuardarSuscripcion(BESuscripciones oBESuscripciones)
        {
            Acceso oDatos;
            string Consulta_SQL;
            bool valor = false;

            oDatos = new Acceso();
            Consulta_SQL = "sp_altaSuscripcion";
            Hdatos = new Hashtable();
            Hdatos.Add("@codCliente", oBESuscripciones.CodigoCliente);
            Hdatos.Add("@descripcionServicio", oBESuscripciones.DetalleServicio);
            Hdatos.Add("@FechaVencimiento", oBESuscripciones.FechaVencimiento);
            Hdatos.Add("@FechaInicio", oBESuscripciones.InicioSuscripcion);
            Hdatos.Add("@Plan", oBESuscripciones.PlanSuscripcion);
            Hdatos.Add("@EmailCliente", oBESuscripciones.Email);

            return oDatos.Escribir(Consulta_SQL, Hdatos);
        }

        public List<BESuscripciones> ListarSuscripciones()
        {
            List<BESuscripciones> listaSuscripcion = new List<BESuscripciones>();

            string Consulta = "sp_listarSuscripciones";
            Hashtable hdatos = new Hashtable();

            DataTable Tabla = oDatos.LeerDataTable(Consulta, hdatos);

            if (Tabla.Rows.Count > 0)
            {
                foreach (DataRow fila in Tabla.Rows)
                {
                    BESuscripciones oBESuscripcion = new BESuscripciones();
                    oBESuscripcion.CodigoCliente = Convert.ToInt32(fila["CodigoCliente"]);
                    oBESuscripcion.Email = Convert.ToString(fila["EmailCliente"]);
                    oBESuscripcion.DetalleServicio = Convert.ToString(fila["Servicio"]);
                    oBESuscripcion.FechaVencimiento = Convert.ToDateTime(fila["FechaVencimiento"]).Date;
                    oBESuscripcion.TipoPlan = Convert.ToString(fila["PlanSuscripcion"]);
                    listaSuscripcion.Add(oBESuscripcion);
                }
            }
            return listaSuscripcion;
        }
    }
}
