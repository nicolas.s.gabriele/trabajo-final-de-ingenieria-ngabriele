﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using DAL;
using System.IO;
using System.Collections;
using System.Data;

namespace MPP
{
    public class MPPMediosDePago
    {
        Acceso oDatos = new Acceso();
        Hashtable Hdatos;

        public List<BEMediosDePago> ListarMediosDePago()
        {
            List<BEMediosDePago> ListarMediosDePago = new List<BEMediosDePago>();

            string Consulta = "sp_listarMediosDePago";

            DataTable Tabla = oDatos.Leer(Consulta);

            if (Tabla.Rows.Count > 0)
            {
                foreach (DataRow fila in Tabla.Rows)
                {
                    BEMediosDePago oBEMP = new BEMediosDePago();
                    oBEMP.Id = Convert.ToInt32(fila["Id"]);
                    oBEMP.Descripcion = Convert.ToString(fila["Descripcion"]);
                    ListarMediosDePago.Add(oBEMP);
                }
            }
            return ListarMediosDePago;
        }
    }
}
