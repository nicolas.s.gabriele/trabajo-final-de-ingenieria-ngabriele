﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BE;
using DAL;

namespace MPP
{
    public class MPPInsumos
    {
        Acceso oDatos = new Acceso();
        Hashtable Hdatos;

        public List<BEInsumos> ListarInsumos()
        {
            List<BEInsumos> ListarInsumos = new List<BEInsumos>();

            string Consulta = "sp_listarInsumos";

            DataTable Tabla = oDatos.Leer(Consulta);

            if (Tabla.Rows.Count > 0)
            {
                foreach (DataRow fila in Tabla.Rows)
                {
                    BEInsumos oBEInsu = new BEInsumos();
                    oBEInsu.Id = Convert.ToInt32(fila["Id"]);
                    oBEInsu.Insumo = Convert.ToString(fila["Insumo"]);
                    ListarInsumos.Add(oBEInsu);
                }
            }
            return ListarInsumos;
        }

        public List<BEInsumos> ListarPrecioPorInsumo(string Insumo)
        {
            List<BEInsumos> listaPrecio = new List<BEInsumos>();

            string Consulta = "sp_ObtenerPrecioInsumo";
            Hashtable hdatos = new Hashtable();
            hdatos.Add("@nombre", Insumo);

            DataTable Tabla = oDatos.LeerDataTable(Consulta, hdatos);

            if (Tabla.Rows.Count > 0)
            {
                foreach (DataRow fila in Tabla.Rows)
                {
                    BEInsumos oBEInsumos = new BEInsumos();
                    oBEInsumos.Precio = Convert.ToInt32(fila["Precio"]);
                    listaPrecio.Add(oBEInsumos);
                }
            }
            return listaPrecio;
        }
    }
}
