﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABS;

namespace BE
{
    public class BEProveedores : Entidad, IEntity
    {
        public int Id { get; set; }
        public string RazonSocial { get; set; }
        public string CUIT { get; set; }
        public DateTime FechaAlta { get; set; }
        public bool Activo { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
    }
}
