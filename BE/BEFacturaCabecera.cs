﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABS;

namespace BE
{
    public class BEFacturaCabecera : Entidad, IEntity
    {
        public int Id { get; set; }
        public string Tipo { get; set; }
        public string CuitEmpresa { get; set; }
        public DateTime InicioActividades { get; set; }
        public string Telefono { get; set; }
        public string TipoDeIva { get; set; }

    }
}
