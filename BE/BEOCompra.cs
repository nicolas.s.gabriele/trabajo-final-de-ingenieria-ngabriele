﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class BEOCompra
    {
        public int Id { get; set; }
        public string Proveedor { get; set; }
        public string Insumo { get; set; }
        public int Cantidad { get; set; }
        public float Precio { get; set; }
    }
}
