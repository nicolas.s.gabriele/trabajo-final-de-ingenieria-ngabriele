﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class BEServicio : Entidad
    {
        public int Id { get; set; }
        public string Servicio { get; set; }
        public float Precio { get; set; }

        public string Descripcion { get; set; }

        public BEServicio(string descripcion, float precio)
        {
            this.Descripcion = descripcion;
            this.Precio = precio;
        }

        public BEServicio(){
        
        }

        public override string ToString()
        {
            return Descripcion + " " + Precio;
        }

    }
}
