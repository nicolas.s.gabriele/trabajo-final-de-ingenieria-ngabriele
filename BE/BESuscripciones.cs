﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class BESuscripciones
    {
        public int Id { get; set; }

        public int CodigoCliente { get; set; }
        public int IdServicio { get; set; }

        public string  DetalleServicio { get; set; }
        public DateTime FechaVencimiento { get; set; }
        public DateTime InicioSuscripcion { get; set; }
        public string PlanSuscripcion { get; set; }
        public string TipoPlan { get; set; }
        public string Email { get; set; }
    }
}
