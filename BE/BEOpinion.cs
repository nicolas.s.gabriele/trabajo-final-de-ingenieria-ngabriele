﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class BEOpinion : Entidad
    {
        public int Id {get;set;}
        public int Estrella { get; set; }
        public string Descripcion { get; set; }
        public string Cliente { get; set; }

        public DateTime Fecha { get; set; }

        public BEUsuario oBEUsuario { get; set; }
    }
}
