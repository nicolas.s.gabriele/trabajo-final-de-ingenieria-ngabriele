﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABS;
using BE;

namespace BE
{
    public class BEUsuario : Entidad, IEntity
    {
            public int ID { get; set; }
            public string Contraseña { get; set; }
            public string Perfil { get; set; }
            public string Nombre { get; set; }
            public string Apellido { get; set; }
            public string Email { get; set; }
            public DateTime FechaIngreso { get; set; }
            public int Contador { get; set; }
            public string Usuario { get; set; }

            public string DNI { get; set; }

        // public List<Componente> Permisos = new List<Componente>();

        public IList<IPerfil> Perfiles { get; }

            public override string ToString()
            {
                return this.Email;
            }
    }
}
