﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABS;

namespace BE
{
    public class BEPagos : Entidad, IEntity
    {
        public string TipoTarjeta { get; set; }
        public string NumeroTarjeta { get; set; }
        public string CodigoSeguridad { get; set; }
        public string Cuotas { get; set; }
    }
}
