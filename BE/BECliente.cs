﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABS;


namespace BE
{
    public class BECliente : Entidad, IEntity
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Dni { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
        public string Perfil { get; set; }
        public bool Activo { get; set; }

        public string Contraseña { get; set; }

        public override string ToString()
        {
            return Nombre + " " + Apellido + " " + Dni + " " + Celular + " " + Email + " " + Contraseña;
        }
    }
}
