﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ABS;

namespace BE
{
    public class BEFacturaDetalle : Entidad, IEntity
    {
        public int Id { get; set; }
        public int IdCabecera { get; set; }
        public int IdItem { get; set; }
        public BECliente oBECliente { get; set; }
        public BEPedidos oBEPedido { get; set; }
        public string Detalle { get; set; }
        public int Cantidad { get; set; }
        public float PrecioTotal { get; set; }
        public int IdPedido { get; set; }
        public string Email { get; set; }
        public string Estado { get; set; }

       

    }
}
