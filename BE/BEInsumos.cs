﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class BEInsumos
    {
        public int Id { get; set; }
        public string Insumo { get; set; }
        public string Descripcion { get; set; }
        public float Precio { get; set; }
        public bool Activo { get; set; }
    }
}
