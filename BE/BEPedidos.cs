﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BE
{
    public class BEPedidos : Entidad
    {
        public BEServicio oBEServicio { get; set; }

        public int Id { get; set; }
        public BEUsuario oBEUsuario { get; set; }
        public string Usuario { get; set; }
        public string servicio { get; set; }
        public string medioDePago { get; set; }
        public string observaciones { get; set; }
        public string rubroEmpresa { get; set; }
        public float precio { get; set; }
        public int cantidad { get; set; }
        public string Estado { get; set; }
        public DateTime Fecha { get; set; }
    }
}
