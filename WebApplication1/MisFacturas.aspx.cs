﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BE;
using SERVICIOS;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.IO;
using iText.Layout.Layout;

namespace WebApplication1
{
    public partial class MisFacturas : System.Web.UI.Page
    {
        BLLFacturacion oBLLFac = new BLLFacturacion();
        BEFacturaDetalle oBEFac = new BEFacturaDetalle();

        private Document _document;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string usuario = Convert.ToString(Session["Usuario"]);
                CargarGrilla();
            }

        }

        void CargarGrilla()
        {
            string usuario = Convert.ToString(Session["Usuario"]);
            GridView1.DataSource = null;
            GridView1.DataSource = oBLLFac.listarFacturasPorCliente(usuario);
            GridView1.DataBind();
        }

        protected void bntGenerar_Click(object sender, EventArgs e)
        {
            GeneratePDF();
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            // Necesario para evitar el error "Control 'GridView1' of type 'GridView' must be placed inside a form tag with runat=server."
        }

        public void GeneratePDF()
        {
            if (GridView1.HeaderRow != null)
            {
                Document document = new Document();
                MemoryStream memoryStream = new MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                document.Open();

                Paragraph header = new Paragraph("Facturas Emitidas", new Font(Font.FontFamily.HELVETICA, 24f, Font.BOLD));
                header.Alignment = Element.ALIGN_CENTER;
                document.Add(header);

                // Crear una tabla para los encabezados
                PdfPTable headerTable = new PdfPTable(GridView1.Columns.Count);
                headerTable.DefaultCell.Padding = 3;
                headerTable.WidthPercentage = 100;
                headerTable.HorizontalAlignment = Element.ALIGN_LEFT;

                // Agregar los encabezados desde las columnas del GridView
                foreach (TableCell gridViewHeaderCell in GridView1.HeaderRow.Cells)
                {
                    PdfPCell headerCell = new PdfPCell(new Phrase(gridViewHeaderCell.Text));
                    headerTable.AddCell(headerCell);
                }

                // Crear una tabla para los datos del GridView
                PdfPTable dataTable = new PdfPTable(GridView1.Columns.Count);
                dataTable.DefaultCell.Padding = 3;
                dataTable.WidthPercentage = 100;
                dataTable.HorizontalAlignment = Element.ALIGN_LEFT;

                // Agregar los datos desde las filas del GridView
                foreach (GridViewRow row in GridView1.Rows)
                {
                    foreach (TableCell cell in row.Cells)
                    {
                        PdfPCell dataCell = new PdfPCell(new Phrase(cell.Text));
                        dataTable.AddCell(dataCell);
                    }
                }

                // Agregar la tabla de encabezados al documento
                document.Add(headerTable);

                // Agregar una separación entre encabezados y datos
                document.Add(new Paragraph(" "));

                // Agregar la tabla de datos al documento
                document.Add(dataTable);

                document.Close();

                // Descargar el PDF en pantalla
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=Factura.pdf");
                Response.OutputStream.Write(memoryStream.GetBuffer(), 0, memoryStream.GetBuffer().Length);
                Response.OutputStream.Flush();
                Response.OutputStream.Close();
                Response.End();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Informe generado con exito');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Ocurrio un error, contacte al administrador.');", true);
            }

        }
    }
}