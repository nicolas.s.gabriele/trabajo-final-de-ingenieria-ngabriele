﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaestras/Site1.Master" AutoEventWireup="true" CodeBehind="Registracion.aspx.cs" Inherits="WebApplication1.Registracion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
     <br />  

     <form id="form2" runat="server">

    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <h1 class="titulo-pagina">Formulario de Registracion</h1>
        </div>
        <div class="col-md-2">
            <asp:CheckBox ID="CheckBox1" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBox1_CheckedChanged" Text="Cliente" ForeColor="Black" />
&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
    </div>
        <div class="row">
            <asp:CheckBox ID="CheckBox2" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBox2_CheckedChanged" Text="Usuario" ForeColor="Black" />
            <br />
            <br /> 
            <div class="col-md-4">
                Email <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>
            </div>
            <div class="col-md-6">
                <br />
            </div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-4">
                Contraseña <asp:TextBox ID="txtpassword" runat="server" class="form-control" type="password" TextMode="Password"></asp:TextBox>
            </div>
            <div class="col-md-6">
                <br />
            </div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-2">
                Repetir
                Contraseña <asp:TextBox ID="txtRepetirPassword" runat="server" class="form-control" type="password" TextMode="Password"></asp:TextBox>
            </div>
            <div class="col-md-8">
                <br />
            </div>
        </div>
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-2">
                Teléfono&nbsp;
                <asp:TextBox ID="txtTelefono" runat="server"></asp:TextBox>
            </div>
        </div>
        <div class="row">
            <div class="col-md-2" style="height: 26px"></div>
            <div class="col-md-2">
                DNI/CUIL
                <asp:TextBox ID="txtDNI" runat="server"></asp:TextBox>
            </div>
            <div class="col-md-8">
                <br />
            </div>
        </div>
        <div class="row">
            <asp:Label ID="Label1" runat="server" Text="Nombre"></asp:Label>
&nbsp;<asp:TextBox ID="txtNombre" runat="server"></asp:TextBox>
        </div>
        <br />

        <div class="row">
           <asp:Label ID="Label2" runat="server" Text="Apellido"></asp:Label>
&nbsp;<asp:TextBox ID="txtApellido" runat="server"></asp:TextBox>

            <div class="col-md-2">
                <br />
                <br />
                <asp:Button ID="Button1" runat="server" CssClass="form-control btn btn-primary" Text="Registrarse" Height="32px" OnClick="Button1_Click1"/>
            </div>
            <div class="col-md-8"></div>
        </div>

     </form>

    <br />
    <br />
</asp:Content>
