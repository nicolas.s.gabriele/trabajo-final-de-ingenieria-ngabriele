﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE;
using BLL;

namespace WebApplication1
{
    public partial class MiSuscripcion : System.Web.UI.Page
    {
        BLLOpinion oBLLOpinion;
        BEOpinion oBEOpinion;
        BLLSuscripciones oBLLSuscripciones;
        BESuscripciones oBESuscripciones;

  
        protected void Page_Load(object sender, EventArgs e)
        {
            oBLLOpinion = new BLLOpinion();
            oBEOpinion = new BEOpinion();
            oBLLSuscripciones = new BLLSuscripciones();
            oBESuscripciones = new BESuscripciones();
            oBEOpinion.Cliente = Session["Usuario"].ToString();
            oBESuscripciones.CodigoCliente = Convert.ToInt32(Session["IDUsuario"]);

            try 
            {
                var precio = oBLLSuscripciones.ListarSuscripcionCliente(oBESuscripciones.CodigoCliente).FirstOrDefault();
                if (precio != null)
                {                    
                    lblFechaRenovacion.Text = Convert.ToString(precio.FechaVencimiento);
                    lblFechaInicio.Text = Convert.ToString(precio.InicioSuscripcion);
                    lblTipoSuscripcion.Text = Convert.ToString(precio.PlanSuscripcion);
                    lblSuscripcion.Text = Convert.ToString(precio.TipoPlan);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('No tiene ninguna suscripcion activa');", true);
                }
                
            }
            catch (Exception)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('No tiene ninguna suscripcion activa');", true);
            }
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            if (txtComentario.Text != null && RadioButtonList1.SelectedItem != null)
            {
                oBEOpinion.Descripcion = txtComentario.Text;
                oBEOpinion.Fecha = DateTime.Now;
                oBEOpinion.Cliente = Session["Usuario"].ToString();
                oBEOpinion.Estrella = Convert.ToInt32(RadioButtonList1.Text);
                oBLLOpinion.Guardar(oBEOpinion);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Su opinion fue creada con exito');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique los datos ingresados');", true);
            }

        }

        protected void btnActualizar_Click(object sender, EventArgs e)
        {

        }
    }
}