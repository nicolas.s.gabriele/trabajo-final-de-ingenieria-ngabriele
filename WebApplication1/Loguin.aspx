﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="Loguin.aspx.cs" Inherits="WebApplication1.Loguin" %>
<!DOCTYPE html>

<html lang="en">
<head>
     <meta charset="utf-8" />
      
        
     <%--<style type="text/css">
         .btn {
             width: 32px;
         }
     </style>--%>
        
</head>
<body> 
    <link href="../CSS/estilo-login.css" rel="stylesheet" />
    <form id="form1" runat="server">
    <div class="wrapper"
        <form action="">
            <h1>Bienvenido a Pixelwave</h1>

            <h4>Por favor ingrese sus datos para acceder al sistema</h4>
            <div class="input-box">
                &nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label ID="Label1" runat="server" Text="Email"></asp:Label>
&nbsp;&nbsp; <asp:TextBox ID="txtUsuario" runat="server" MaxLength="50"></asp:TextBox>
                <br />
            </div>
            <div class="input-box">
                <asp:Label ID="Label2" runat="server" Text="Contraseña"></asp:Label>
&nbsp;&nbsp;<asp:TextBox ID="txtContraseña" runat="server" TextMode="Password" MaxLength="15"></asp:TextBox>
                <br />
            </div>
            <div class="remember-forgot">
                <asp:Button ID="Button1" runat="server" Text="Login" OnClick="Button1_Click" Height="30px" Width="72px"/>

                <label>&nbsp;<asp:CheckBox ID="CheckCliente" runat="server" Text="Soy Cliente" /></label>
            </div>

            <div class="register-link">
                <p>No tenes cuenta? <a 
                    href="Registracion.aspx">Registrate</a></p>                
            </div>
        </form>
    </form>
</body>   
</html>