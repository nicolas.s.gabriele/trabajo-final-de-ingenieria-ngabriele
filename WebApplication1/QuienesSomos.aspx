﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaestras/Site1.Master" AutoEventWireup="true" CodeBehind="QuienesSomos.aspx.cs" Inherits="WebApplication1.QuienesSomos"%>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" >
        <link href="CSS/estilos-quienes-somos.css" rel="stylesheet" />

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="descripcion-quienes-somos">
            <p>PixelWave Marketing es una empresa dedicada a la prestación de servicios de marketing digital</p> 
            <p>para pequeñas empresas, que permite generar contenido, aumentar el volumen de ventas e involucrar y construir relaciones
            con el consumidor.</p><br />
            <p>Nos enfocamos dentro del ámbito de la República Argentina, focalizando especialmente en CABA, donde consideramos que se</p> 
            <p>encuentra el mayor potencial de mercado.</p>
            <p>Hacemos hincapié principalmente en pequeñas empresas, emprendedores y negocios con potencial de crecimiento.</p><br />
        </div>
        </div>
    <div class="col-md-2"></div>
</div>
<br />

</asp:Content>