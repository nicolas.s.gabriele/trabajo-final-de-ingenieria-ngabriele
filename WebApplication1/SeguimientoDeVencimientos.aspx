﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaestras/Site1.Master" AutoEventWireup="true" CodeBehind="SeguimientoDeVencimientos.aspx.cs" Inherits="WebApplication1.SeguimientoDeVencimientos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <asp:GridView ID="GridView1" runat="server" CellPadding="4" AutoGenerateColumns="False" ForeColor="#333333" GridLines="None" Width="661px">
            <Columns>
                <asp:BoundField DataField="CodigoCliente" HeaderText="Codigo Cliente" />
                <asp:BoundField DataField="Email" HeaderText="Email Cliente" />
                <asp:BoundField DataField="DetalleServicio" HeaderText="Servicio" />
                <asp:BoundField DataField="FechaVencimiento" HeaderText="Vencimiento" />
                <asp:BoundField DataField="TipoPlan" HeaderText="Suscripcion" />
            </Columns>
            <AlternatingRowStyle BackColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
    </form>
</asp:Content>
