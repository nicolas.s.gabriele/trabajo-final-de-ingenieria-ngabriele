﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;
using BLL;
using BE;
using SERVICIOS;

namespace WebApplication1
{
    public partial class ReportesContrataciones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
        }
        BLLPedido oBLLPedidos = new BLLPedido();

        void CargarGrafico()
        {
            Dictionary<string, double> ListaDatos = new Dictionary<string, double>();

            foreach (BEPedidos servicio in new BLLServicio().ServiciosContratadosPorMes(Convert.ToInt32(Calendar1.SelectedDate.Month), Convert.ToInt32(Calendar1.SelectedDate.Year)))
            {
                ListaDatos.Add(servicio.servicio, servicio.cantidad);
            }

            Chart1.Titles.Clear();
            Chart1.ChartAreas.Clear();
            Chart1.Series.Clear();

            Title Titulo = new Title("Cantidad de contrataciones por Servicio");
            //Titulo.Font = new Font("Tahoma", 15, FontStyle.Bold);
            Chart1.Titles.Add(Titulo);
            
            ChartArea Area = new ChartArea();
            Area.Area3DStyle.Enable3D = false;
            Chart1.ChartAreas.Add(Area);

            Series serie = new Series("Total");
            serie.ChartType = SeriesChartType.Line;
            serie.Points.DataBindXY(ListaDatos.Keys, ListaDatos.Values);

            Chart1.Series.Add(serie);
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            CargarGrafico();
        }
    }
}