﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BE;


namespace WebApplication1
{
    public partial class Facturacion : System.Web.UI.Page
    {
        BEPedidos oBEPed;
        private int Id;
        private String servicio;
        private String usuario;
        private int precio;

        public void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BEPedidos oBEPed = new BEPedidos();
                cargarLista();
                txtNumeroTarjeta.MaxLength = 16;
                txtCodSeg.MaxLength = 4;
            }
            else
            {
                txtNumeroTarjeta.MaxLength = 16;
                txtCodSeg.MaxLength = 4;
            }
           
        }

        public void Limpiar()
        {
            txtNumeroTarjeta.Text = "";
            txtCodSeg.Text = "";
        }
        public void cargarLista()
        {
            BLLPedido oBLLPedidos = new BLLPedido();
            GridView1.DataSource = null;
            GridView1.DataSource = oBLLPedidos.ListarPedidos();
            GridView1.DataBind();
        }
        
        public void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            BEPedidos oBEPed = new BEPedidos();
            oBEPed.Id = Convert.ToInt32(GridView1.SelectedRow.Cells[1].Text);
            oBEPed.servicio = GridView1.SelectedRow.Cells[3].Text.ToString();
            oBEPed.precio = Convert.ToInt32(GridView1.SelectedRow.Cells[4].Text.ToString());
            oBEPed.Usuario = GridView1.SelectedRow.Cells[2].Text.ToString();
        }


        public void Button1_Click(object sender, EventArgs e)
        {
            if (txtNumeroTarjeta.Text != "" && txtCodSeg.Text != "" && GridView1.SelectedRow != null)
            {
                Id = Convert.ToInt32(GridView1.SelectedRow.Cells[1].Text);
                servicio = GridView1.SelectedRow.Cells[3].Text.ToString();
                precio = Convert.ToInt32(GridView1.SelectedRow.Cells[4].Text.ToString());
                usuario = GridView1.SelectedRow.Cells[2].Text.ToString();

                BLLFacturacion oBLLFacturacion = new BLLFacturacion();
                BLLSuscripciones oBLLSuscripciones = new BLLSuscripciones();
                BLLPedido oBLLPedido = new BLLPedido();
                BLLCliente oBLLCliente = new BLLCliente();
                BEFacturaCabecera oBEFacCab = new BEFacturaCabecera();
                BEFacturaDetalle oBEFacDet = new BEFacturaDetalle();
                BEPagos oBEPagos = new BEPagos();
                BESuscripciones oBESuscripciones = new BESuscripciones();

                oBEFacDet.Detalle = servicio;
                oBEFacDet.PrecioTotal = precio;
                oBEFacDet.IdPedido = Id;
                oBEPagos.NumeroTarjeta = txtNumeroTarjeta.Text;
                oBEPagos.CodigoSeguridad = txtCodSeg.Text;
                oBEPagos.Cuotas= DDLCuotas.Text;
                oBEPagos.TipoTarjeta = DDLCredito.Text;
                oBLLFacturacion.Guardar(oBEFacCab, oBEFacDet, oBEPagos);
                oBLLPedido.actualizarPedido(Id);
                var codigoCliente = oBLLCliente.ListarCodigoCliente(usuario).FirstOrDefault();
                oBESuscripciones.CodigoCliente = Convert.ToInt32(codigoCliente.Id);
                oBESuscripciones.DetalleServicio = servicio;
                oBESuscripciones.InicioSuscripcion = DateTime.Now.Date;
                oBESuscripciones.FechaVencimiento = oBESuscripciones.InicioSuscripcion.AddYears(1);
                oBESuscripciones.PlanSuscripcion = "Anual";
                oBESuscripciones.Email = usuario;
                oBLLSuscripciones.GuardarSuscripcion(oBESuscripciones);
                cargarLista();
                Limpiar();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('El Pedido fue facturado con exito');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique que haya completado todos los datos');", true);
            }
        }
    }
}