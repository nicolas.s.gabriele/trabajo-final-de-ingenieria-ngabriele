﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SERVICIOS;

namespace WebApplication1
{
    public partial class Logout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Clear();
            var Usuario = ServicioLogin.ObtenerSesion.Usuario;
            ServicioLogin Logout = ServicioLogin.ObtenerSesion;
            Logout.Logout();
            Response.Redirect(@"~\Loguin.aspx");
        }
    }
}