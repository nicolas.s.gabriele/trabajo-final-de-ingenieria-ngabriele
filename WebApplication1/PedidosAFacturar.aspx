﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaestras/Site1.Master" AutoEventWireup="true" CodeBehind="PedidosAFacturar.aspx.cs" Inherits="WebApplication1.Facturacion" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <h3>Pedidos a Facturar</h3>
        <br />
        <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" Width="682px" AutoGenerateColumns="False" AllowPaging="True" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
            <Columns>
                <asp:ButtonField ButtonType="Button" ControlStyle-CssClass="btn btn-primary"  CommandName="Select" ShowHeader="True" Text="Seleccionar" />
                <asp:BoundField DataField="Id" HeaderText="Id" />
                <asp:BoundField DataField="Usuario" HeaderText="Usuario" />
                <asp:BoundField DataField="Servicio" HeaderText="Servicio" />
                <asp:BoundField DataField="Precio" HeaderText="Precio" />
                <asp:BoundField DataField="Fecha" HeaderText="Fecha" />
            </Columns>
            <AlternatingRowStyle BackColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
                     <br />
        <br />
        <br />
        <asp:Label ID="Label2" runat="server" Text="Tipo Tarjeta"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="DDLCredito" runat="server" Height="36px" Width="73px">
            <asp:ListItem>Credito</asp:ListItem>
            <asp:ListItem>Debito</asp:ListItem>
        </asp:DropDownList>
&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label1" runat="server" Text="Numero Tarjeta"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtNumeroTarjeta" runat="server" Height="36px" Width="167px" MaxLength="18"></asp:TextBox>
&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label ID="Label3" runat="server" Text="Codigo Seguridad"></asp:Label>
&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="txtCodSeg" runat="server" Height="36px" Width="87px" MaxLength="4" TextMode="Password"></asp:TextBox>
        <br />
        <br />
        Cuotas&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="DDLCuotas" runat="server" Height="36px" Width="47px">
            <asp:ListItem>1</asp:ListItem>
            <asp:ListItem>3</asp:ListItem>
            <asp:ListItem>6</asp:ListItem>
        </asp:DropDownList>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" Height="36px" OnClick="Button1_Click" Text="Generar Factura" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" />
    </form>
</asp:Content>
