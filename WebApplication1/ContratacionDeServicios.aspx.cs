﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE;
using BLL;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using SERVICIOS;

namespace WebApplication1
{
    public partial class ContratacionDeServicios : System.Web.UI.Page
    {
         BEPedidos oBEpedidos;
         BLLPedido oBLLPedido;
         BEUsuario user;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                user = ServicioLogin.ObtenerSesion.Usuario;
                oBLLPedido = new BLLPedido();
                oBEpedidos = new BEPedidos();
                cargarLista();
            }   
        }
        private String urlBackup;

        private void LimpiarGridView()
        {
            GridView1.DataSource = null;
            GridView1.DataBind();
        }

        public void cargarLista()
        {
            BLLMediosDePago oBLLMedioPago = new BLLMediosDePago();
            BLLServicio oBLLServicio = new BLLServicio();
            BLLRubro oBLLRubro = new BLLRubro();

            DropDownList2.DataSource = null;
            DropDownList2.DataSource = oBLLMedioPago.ListarMediosDePago();
            DropDownList2.DataValueField = "Descripcion";
            DropDownList2.DataTextField = "Descripcion";
            DropDownList2.DataBind();

            DropDownList3.DataSource = null;
            DropDownList3.DataSource = oBLLServicio.ListarTodo();
            DropDownList3.DataValueField = "Servicio";
            DropDownList3.DataTextField = "Servicio";
            DropDownList3.DataBind();

            DropDownList4.DataSource = null;
            DropDownList4.DataSource = oBLLRubro.ListarRubros();
            DropDownList4.DataValueField = "Descripcion";
            DropDownList4.DataTextField = "Descripcion";
            DropDownList4.DataBind();
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            
        }
        protected void btnAgregarItem_Click(object sender, EventArgs e)
        {
            //BEServicio oBEServicio = new BEServicio();

           

            string valor1 = Convert.ToString(DropDownList3.SelectedValue);
            string valor2 = Convert.ToString(DropDownList4.SelectedValue);
            string valor3 = Convert.ToString(DropDownList2.SelectedValue);
            string valor4 = TextBox1.Text;

            if (GridView1.DataSource == null)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Servicio a Contratar"); 
                dt.Columns.Add("Rubro de su empresa");
                dt.Columns.Add("Medio de Pago");
                dt.Columns.Add("Observaciones");                
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }

             DataTable datosTabla = (DataTable)GridView1.DataSource;

            DataRow nuevaFila = datosTabla.NewRow();
            nuevaFila["Servicio a Contratar"] = valor1; 
            nuevaFila["Rubro de su empresa"] = valor2;
            nuevaFila["Medio de Pago"] = valor3;
            nuevaFila["Observaciones"] = valor4;
            datosTabla.Rows.Add(nuevaFila);

            GridView1.DataBind();
        }

        public void btnGenPedido_Click(object sender, EventArgs e)
        {
            BLLServicio oBLLServicio = new BLLServicio();
            if (DropDownList3.SelectedValue != null && TextBox1.Text != "")
            {
                BLLPedido oBLLPedido = new BLLPedido();
                BEPedidos oBEPedidos = new BEPedidos();
                oBEPedidos.Usuario = Session["Usuario"].ToString();
                oBEPedidos.servicio = Convert.ToString(DropDownList3.SelectedValue);
                oBEPedidos.rubroEmpresa = Convert.ToString(DropDownList4.SelectedValue);
                oBEPedidos.medioDePago = Convert.ToString(DropDownList2.SelectedValue);
                oBEPedidos.observaciones = TextBox1.Text;
                oBEPedidos.Fecha = DateTime.Now.Date;
                //tengo que guardar el cliente que genero el pedido.
                var precio = oBLLServicio.ListarPrecioPorServicio(oBEPedidos.servicio).FirstOrDefault();
                Session["precio"] = precio;
                oBEPedidos.precio = precio.Precio;
                oBLLPedido.Guardar(oBEPedidos);
                TextBox1.Text = "";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Su Pedido fue creado con exito');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique que completado todos los datos del pedido');", true);
            }        
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            GeneratePDF();
        }

        public decimal CalcularTotal()
        {
            BLLServicio oBLLServicio = new BLLServicio();
            BEPedidos oBEPedidos = new BEPedidos();
            oBEPedidos.servicio = Convert.ToString(DropDownList3.SelectedValue);
            var precio = oBLLServicio.ListarPrecioPorServicio(oBEPedidos.servicio).FirstOrDefault();
            return Convert.ToDecimal(precio.Precio);
        }

        public void GeneratePDF()
        {
            if (GridView1.HeaderRow != null)
            {
                Document document = new Document();
                MemoryStream memoryStream = new MemoryStream();
                PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
                document.Open();

                Paragraph header = new Paragraph("Pedido", new Font(Font.FontFamily.HELVETICA, 24f, Font.BOLD));
                header.Alignment = Element.ALIGN_CENTER;
                document.Add(header);

                // Crear una tabla para los encabezados
                PdfPTable headerTable = new PdfPTable(GridView1.Columns.Count);
                headerTable.DefaultCell.Padding = 3;
                headerTable.WidthPercentage = 100;
                headerTable.HorizontalAlignment = Element.ALIGN_LEFT;

                // Agregar los encabezados desde las columnas del GridView
                foreach (TableCell gridViewHeaderCell in GridView1.HeaderRow.Cells)
                {
                    PdfPCell headerCell = new PdfPCell(new Phrase(gridViewHeaderCell.Text));
                    headerTable.AddCell(headerCell);
                }

                // Crear una tabla para los datos del GridView
                PdfPTable dataTable = new PdfPTable(GridView1.Columns.Count);
                dataTable.DefaultCell.Padding = 3;
                dataTable.WidthPercentage = 100;
                dataTable.HorizontalAlignment = Element.ALIGN_LEFT;

                // Agregar los datos desde las filas del GridView
                foreach (GridViewRow row in GridView1.Rows)
                {
                    foreach (TableCell cell in row.Cells)
                    {
                        PdfPCell dataCell = new PdfPCell(new Phrase(cell.Text));
                        dataTable.AddCell(dataCell);
                    }
                }

                // Agregar la tabla de encabezados al documento
                document.Add(headerTable);

                // Agregar una separación entre encabezados y datos
                document.Add(new Paragraph(" "));

                // Agregar la tabla de datos al documento
                document.Add(dataTable);

                decimal total = CalcularTotal(); // Debes implementar esta función para calcular el total.
                string totalEnPesos = $"{total:N2} ARS";
                Paragraph totalParagraph = new Paragraph($"Importe Total: {totalEnPesos}", new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD));
                totalParagraph.Alignment = Element.ALIGN_RIGHT;
                document.Add(totalParagraph);

                document.Close();

                // Descargar el PDF en pantalla
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=Factura.pdf");
                Response.OutputStream.Write(memoryStream.GetBuffer(), 0, memoryStream.GetBuffer().Length);
                Response.OutputStream.Flush();
                Response.OutputStream.Close();
                Response.End();
                LimpiarGridView();
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique que haya seleccionado el item del pedido');", true);
            }
            
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}