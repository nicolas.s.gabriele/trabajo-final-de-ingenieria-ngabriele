﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE;
using BLL;

namespace WebApplication1
{
    public partial class GestionServicios : System.Web.UI.Page
    {
        BLLServicio oBLLServicio;
        BEServicio oBEServicio;
        private int urlBackup;
        protected void Page_Load(object sender, EventArgs e)
        {
            oBLLServicio = new BLLServicio();
            oBEServicio = new BEServicio();
            CargarGrilla();
        }

        protected void txtConfirmar_Click(object sender, EventArgs e)
        {
            //string campoPrecio = txtPrecio.Text;
            //bool respuesta = Regex.IsMatch(campoPrecio, "^([0-9]+$)");

            if (txtNombre.Text != null && txtDescripcion != null && txtPrecio.Text != null)
            {
                try
                {
                    oBEServicio.Servicio = txtNombre.Text;
                    oBEServicio.Descripcion = txtDescripcion.Text;
                    oBEServicio.Precio = Convert.ToInt32(txtPrecio.Text);
                    oBLLServicio.Guardar(oBEServicio);
         
                }
                catch (Exception ex)
                {
                    
                }
            }
            else
            {
               
            }
        }

        void CargarGrilla()
        {
            GridView1.DataSource = null;
            GridView1.DataSource = oBLLServicio.ListarTodo();
            GridView1.DataBind();
        }

        protected void btnBaja_Click(object sender, EventArgs e)
        {
            try
            {
                if (GridView1.SelectedRow == null)
                {
                    throw new Exception("Debe seleccionar una fila!");
                }
                else
                {
                    oBEServicio.Id = Convert.ToInt32(GridView1.SelectedRow.Cells[1].Text.ToString());


                    BLLServicio oBLLServicio = new BLLServicio();

                    oBLLServicio.Baja(oBEServicio);
                    CargarGrilla();
                }
                
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique los datos ingresados');", true);
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtNombre.Text = GridView1.SelectedRow.Cells[2].Text.ToString();
            txtDescripcion.Text = GridView1.SelectedRow.Cells[4].Text.ToString();
            txtPrecio.Text = GridView1.SelectedRow.Cells[3].Text.ToString();
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            if (txtNombre.Text != null && txtDescripcion != null && txtPrecio.Text != null)
            {
                try
                {
                    oBEServicio.Id = Convert.ToInt32(GridView1.SelectedRow.Cells[1].Text.ToString());
                    oBEServicio.Servicio = txtNombre.Text;
                    oBEServicio.Descripcion = txtDescripcion.Text;
                    oBEServicio.Precio = Convert.ToInt32(txtPrecio.Text);
                    oBLLServicio.Guardar(oBEServicio);
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique los datos ingresados');", true);
                }
            }
            else
            {

            }
        }
    }
}