﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaestras/Site1.Master" AutoEventWireup="true" CodeBehind="GestionUsuario.aspx.cs" Inherits="WebApplication1.GestionUsuario" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
    <asp:Label ID="Label5" runat="server" Text="Apellido"></asp:Label>
&nbsp;<asp:TextBox ID="txtApellido" runat="server" Height="28px" Width="172px"></asp:TextBox>
    &nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label6" runat="server" Text="Nombre"></asp:Label>
&nbsp;<asp:TextBox ID="txtNombre" runat="server" Height="28px" Width="171px"></asp:TextBox>
    &nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label8" runat="server" Text="DNI"></asp:Label>
&nbsp;<asp:TextBox ID="txtDni" runat="server" Height="28px" Width="195px"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="Label7" runat="server" Text="Email"></asp:Label>
&nbsp;<asp:TextBox ID="txtEmail" runat="server" Height="28px" Width="189px"></asp:TextBox>
    &nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label4" runat="server" Text="Contraseña"></asp:Label>
&nbsp;<asp:TextBox ID="txtContraseña" runat="server" TextMode="Password" Height="28px" Width="152px"></asp:TextBox>
    &nbsp;&nbsp;&nbsp;
    <asp:Label ID="Label9" runat="server" Text="Repetir Contraseña"></asp:Label>
&nbsp;<asp:TextBox ID="txtRepCont" runat="server" TextMode="Password" Height="28px" Width="107px"></asp:TextBox>
                
    <br />
        <br />
        <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" Width="721px" AutoGenerateColumns="False" AllowPaging="True" CssClass="centrado" OnRowDataBound="GridView1_RowDataBound" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
            <Columns>
                <asp:ButtonField ButtonType="Button" ControlStyle-CssClass="btn btn-primary"  CommandName="Select" ShowHeader="True" Text="Seleccionar" /> 
                <asp:BoundField DataField="ID" HeaderText="Codigo Usuario" />
                <asp:BoundField DataField="Dni" HeaderText="Dni" />
                <asp:BoundField DataField="Apellido" HeaderText="Apellido" />
                <asp:BoundField DataField="Nombre" HeaderText="Nombre" />
                <asp:BoundField DataField="Email" HeaderText="Email" />
                <asp:BoundField DataField="Contraseña" HeaderText="Contraseña" />                
            </Columns>
            <AlternatingRowStyle BackColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
    <br />
    <br />
    <asp:Button ID="bntConfirmar" runat="server" Text="Confirmar" OnClick="bntConfirmar_Click" Height="36px" Width="93px" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnModificar" runat="server" Height="36px" Text="Modificar" Width="93px" OnClick="btnModificar_Click" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnBaja" runat="server" Height="36px" Text="Baja" Width="93px" OnClick="btnBaja_Click" />
</form>
</asp:Content>
