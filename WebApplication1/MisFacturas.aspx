﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaestras/Site1.Master" AutoEventWireup="true" CodeBehind="MisFacturas.aspx.cs" Inherits="WebApplication1.MisFacturas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <form id="form1" runat="server">
        <br />
&nbsp;&nbsp;&nbsp;<br />
        <h3>Facturas Emitidas</h3>
    <br />
    <asp:GridView ID="GridView1" AutoGenerateColumns="False" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" Width="708px" Height="151px">
            <Columns>
                <asp:BoundField DataField="IdCabecera" HeaderText="Nro Factura" />
                <asp:BoundField DataField="Detalle" HeaderText="Servicio" />
                <asp:BoundField DataField="PrecioTotal" HeaderText="Precio" />
                <asp:BoundField DataField="IdPedido" HeaderText="Pedido" />
                <asp:BoundField DataField="Email" HeaderText="Usuario" />
                <asp:BoundField DataField="Estado" HeaderText="Estado" />
            </Columns>
            <AlternatingRowStyle BackColor="White" />
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
        <br />
        <br />
        <asp:Button ID="bntGenerar" runat="server" Text="Generar PDF" OnClick="bntGenerar_Click" />
    <br />
</form>

</asp:Content>
