﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BE;
using System.Data;

namespace WebApplication1
{
    public partial class GestionCompra : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            cargarLista();
        }

        public void cargarLista()
        {
            BLLProveedores oBLLProveedores = new BLLProveedores();
            BLLInsumos oBLLInsumos = new BLLInsumos();

            DropDownList1.DataSource = null;
            DropDownList1.DataSource = oBLLProveedores.ListarProveedores();
            DropDownList1.DataValueField = "RazonSocial";
            DropDownList1.DataTextField = "RazonSocial";
            DropDownList1.DataBind();

            DropDownList2.DataSource = null;
            DropDownList2.DataSource = oBLLInsumos.ListarInsumos();
            DropDownList2.DataValueField = "Insumo";
            DropDownList2.DataTextField = "Insumo";
            DropDownList2.DataBind();
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            string valor1 = Convert.ToString(DropDownList1.SelectedValue);
            string valor2 = Convert.ToString(DropDownList2.SelectedValue);
            int valor3 = Convert.ToInt32(TextBox1.Text);

            if (GridView1.DataSource == null)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Proveedor");
                dt.Columns.Add("Insumo");
                dt.Columns.Add("Cantidad");
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }

            DataTable datosTabla = (DataTable)GridView1.DataSource;

            DataRow nuevaFila = datosTabla.NewRow();
            nuevaFila["Proveedor"] = valor1;
            nuevaFila["Insumo"] = valor2;
            nuevaFila["Cantidad"] = valor3;
            datosTabla.Rows.Add(nuevaFila);

            GridView1.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            BLLInsumos oBLLInsumos = new BLLInsumos();
            if (DropDownList1.SelectedValue != null && TextBox1.Text != "")
            {
                BLLOCompra oBLLOCompra = new BLLOCompra();
                BEOCompra oBEOCompra = new BEOCompra();
                // oBEPedidos.Usuario = Session["Usuario"].ToString();
                oBEOCompra.Proveedor = Convert.ToString(DropDownList1.SelectedValue);
                oBEOCompra.Insumo = Convert.ToString(DropDownList2.SelectedValue);
                oBEOCompra.Cantidad = Convert.ToInt32(TextBox1.Text);
                var precio = oBLLInsumos.ListarPrecioPorInsumo(oBEOCompra.Insumo).FirstOrDefault();
                oBEOCompra.Precio = precio.Precio;
                oBEOCompra.Precio = precio.Precio * Convert.ToInt32(TextBox1.Text);
                //tengo que guardar el cliente que genero el pedido.
                
                oBLLOCompra.Guardar(oBEOCompra);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Su orden de Compra Fue creada con exito');", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique que completado todos los datos del pedido');", true);
            }
        }
    }
}