﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE;
using BLL;

namespace WebApplication1
{
    public partial class SeguimientoDeVencimientos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string usuario = Convert.ToString(Session["Usuario"]);
                CargarGrilla();
            }
        }

        BLLSuscripciones oBLLSus = new BLLSuscripciones();
        BESuscripciones oBEFac = new BESuscripciones();

        void CargarGrilla()
        {
            string usuario = Convert.ToString(Session["Usuario"]);
            GridView1.DataSource = null;
            GridView1.DataSource = oBLLSus.ListarSuscripciones();
            GridView1.DataBind();
        }
    }
}