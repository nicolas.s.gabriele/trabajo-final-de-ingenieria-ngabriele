﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BE;
using SERVICIOS;

namespace WebApplication1
{
    public partial class GestionClientes : System.Web.UI.Page
    {
        BLLCliente oBLLCliente;
        BECliente oBECliente;
        protected void Page_Load(object sender, EventArgs e)
        {
            oBLLCliente = new BLLCliente();
            oBECliente = new BECliente();
            CargarGrilla();
        }

        void CargarGrilla()
        {
            GridView1.DataSource = null;
            GridView1.DataSource = oBLLCliente.ListarClientes();
            GridView1.DataBind();
        }

        void Limpiar()
        {
            txtDni.Text = null;
            txtApellido.Text = null;
            txtNombre.Text = null;
            txtEmail.Text = null;
            txtCelular.Text = null;
            txtContraseña.Text = null;
        }

        protected void btnConfirmar_Click(object sender, EventArgs e)
        {
            if (txtDni.Text != "" && txtApellido.Text != "" && txtNombre.Text != "" && txtEmail.Text != "" && txtCelular.Text != "" && txtContraseña.Text != "" && GridView1.SelectedRow == null)
            {
                try
                {
                    oBECliente.Dni = txtDni.Text;
                    oBECliente.Apellido = txtApellido.Text;
                    oBECliente.Nombre = txtNombre.Text;
                    oBECliente.Email = txtEmail.Text;
                    oBECliente.Celular = txtCelular.Text;
                    oBECliente.Contraseña = SERVICIOS.ServicioEncriptado.Encriptar(txtContraseña.Text);
                    oBLLCliente.Guardar(oBECliente);
                    CargarGrilla();
                    Limpiar();
                }
                catch (Exception)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique los datos ingresados');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique los datos ingresados');", true);
            }
            
            
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtDni.Text = GridView1.SelectedRow.Cells[1].Text.ToString();
            txtApellido.Text = GridView1.SelectedRow.Cells[2].Text.ToString();
            txtNombre.Text = GridView1.SelectedRow.Cells[3].Text.ToString();
            txtEmail.Text = GridView1.SelectedRow.Cells[4].Text.ToString();
            txtCelular.Text = GridView1.SelectedRow.Cells[5].Text.ToString();            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (txtDni.Text != "" && txtApellido.Text != "" && txtNombre.Text != "" && txtEmail.Text != "" && txtCelular.Text != "" && txtContraseña.Text != "" &&  GridView1.SelectedRow != null)
            {
                try
                {
                    oBECliente.Dni = txtDni.Text;
                    oBECliente.Apellido = txtApellido.Text;
                    oBECliente.Nombre = txtNombre.Text;
                    oBECliente.Email = txtEmail.Text;
                    oBECliente.Celular = txtCelular.Text;
                    oBECliente.Contraseña = SERVICIOS.ServicioEncriptado.Encriptar(txtContraseña.Text);
                    oBLLCliente.Modificar(oBECliente);
                    CargarGrilla();
                    Limpiar();
                }
                catch (Exception)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique los datos ingresados');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique los datos ingresados');", true);
            }
            
           
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
                GridView gv = (GridView)sender;
                gv.PageIndex = e.NewPageIndex;
        }

        protected void btnBaja_Click(object sender, EventArgs e)
        {
            if (GridView1.SelectedRow == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor seleccione un cliente.');", true);
            }
            else
            {
                oBECliente.Dni = GridView1.SelectedRow.Cells[1].Text.ToString();
                oBLLCliente.Baja(oBECliente);
                Limpiar();
                CargarGrilla();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('El cliente fue dado de baja con exito.');", true);
            }
        }
    }
}