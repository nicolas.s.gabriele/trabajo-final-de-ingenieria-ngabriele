﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE;
using BLL;
using SERVICIOS;

namespace WebApplication1
{
    public partial class GestionUsuario : System.Web.UI.Page
    {
        BLLUsuario oBLLUsuario;
        BEUsuario oBEUsuario;

        protected void Page_Load(object sender, EventArgs e)
        {
            oBLLUsuario = new BLLUsuario();
            oBEUsuario = new BEUsuario();
            CargarGrilla();
        }

        void CargarGrilla()
        {
            GridView1.DataSource = null;
            GridView1.DataSource = oBLLUsuario.ListarUsuarios();
            GridView1.DataBind();
        }

        void Limpiar()
        {
            txtEmail.Text = null;
            txtApellido.Text = null;
            txtNombre.Text = null;
            txtRepCont.Text = null;
            txtContraseña.Text = null;
            txtDni.Text = null;
        }

        protected void bntConfirmar_Click(object sender, EventArgs e)
        {
            if (txtDni.Text != "" && txtApellido.Text != "" && txtNombre.Text != "" && txtEmail.Text != "" && GridView1.SelectedRow == null && txtContraseña.Text == txtRepCont.Text)
            {
                try
                {
                    oBEUsuario.Email = txtEmail.Text;
                    oBEUsuario.Nombre = txtNombre.Text;
                    oBEUsuario.Apellido = txtApellido.Text;
                    oBEUsuario.Contraseña = SERVICIOS.ServicioEncriptado.Encriptar(txtContraseña.Text);
                    oBEUsuario.DNI = txtDni.Text;
                    oBEUsuario.FechaIngreso = DateTime.Now;
                    oBLLUsuario.InsertarUsuario(oBEUsuario);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('El usuario a sido dado de alta con exito.');", true);
                    Limpiar();

                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique los datos ingresados');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique los datos ingresados');", true);
            }
        }

        protected void btnBaja_Click(object sender, EventArgs e)
        {
            if (GridView1.SelectedRow == null)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor seleccione un usuario.');", true);
            }
            else
            {
                oBEUsuario.ID = Convert.ToInt32(GridView1.SelectedRow.Cells[1].Text.ToString());
                oBLLUsuario.BajaUsuario(oBEUsuario);
                Limpiar();
                CargarGrilla();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('El usuario fue dado de baja con exito.');", true);
            }
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            if (txtDni.Text != "" && txtApellido.Text != "" && txtNombre.Text != "" && txtEmail.Text != "" && GridView1.SelectedRow != null)
            {
                try
                {
                    oBEUsuario.ID = Convert.ToInt32(GridView1.SelectedRow.Cells[1].Text.ToString());
                    oBEUsuario.DNI = txtDni.Text;
                    oBEUsuario.Apellido = txtApellido.Text;
                    oBEUsuario.Nombre = txtNombre.Text;
                    oBEUsuario.Email = txtEmail.Text;
                    oBLLUsuario.Modificar(oBEUsuario);
                    CargarGrilla();
                    Limpiar();
                }
                catch (Exception)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique los datos ingresados');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique los datos ingresados');", true);
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            txtDni.Text = GridView1.SelectedRow.Cells[2].Text.ToString();
            txtApellido.Text = GridView1.SelectedRow.Cells[3].Text.ToString();
            txtNombre.Text = GridView1.SelectedRow.Cells[4].Text.ToString();
            txtEmail.Text = GridView1.SelectedRow.Cells[5].Text.ToString();
        }

        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Reemplaza el valor real con asteriscos
                string nombreCampo = DataBinder.Eval(e.Row.DataItem, "Contraseña") as string;
                if (!string.IsNullOrEmpty(nombreCampo))
                {
                    e.Row.Cells[6].Text = new string('*', nombreCampo.Length);
                }
            }
        }
    }
}