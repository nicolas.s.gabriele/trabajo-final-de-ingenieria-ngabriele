﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BE;
using BLL;
using SERVICIOS;


namespace WebApplication1
{
    public partial class Loguin : System.Web.UI.Page 
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Add("usuario", txtUsuario.Text);
            var SL = ServicioLogin.ObtenerSesion;
        }
        private string usuario_x;
        public string usuario
        {
            get
            {
                return usuario_x;
            }
            set
            {
                usuario_x = value;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            int id = 0;
            string nombreusuario = txtUsuario.Text;
            int cantidadusername = Request.Form["txtUsuario"].Length;
            int cantidadpassword = Request.Form["txtContraseña"].Length;
            string email = Request.Form["txtUsuario"];
            string contraseña = Request.Form["txtContraseña"];


            //Verificar los campos antes de intentar el login
            if (cantidadusername >= 8 && cantidadusername <= 50 && cantidadpassword >= 8 && cantidadpassword <= 20 && !String.IsNullOrEmpty(email) && !String.IsNullOrEmpty(contraseña))
            {
               
                if (CheckCliente.Checked== true)
                {
                    BLL.BLLUsuario oBLLLogin = new BLL.BLLUsuario();
                    BECliente oBECliente = oBLLLogin.LogearCliente(id, email, ServicioEncriptado.Encriptar(contraseña));
                    //Login exitoso
                    if (oBECliente != null)
                    {
                        ServicioLogin.LoginCliente(oBECliente);
                        oBLLLogin.Logear(id, nombreusuario, contraseña);
                        Session["logueado"] = true;
                        Session["statusDB"] = "";
                        Session["perfil"] = oBECliente.Perfil;
                        Session["Usuario"] = oBECliente.Email;
                        Session["IDUsuario"] = oBECliente.Id;
                        Response.Redirect(@"~\Home.aspx");

                        //Como el login fue exitoso, verifica la integridad de la base de datos
                        if (Session["logueado"] != null)
                        {
                            Session["Usuario"] = oBECliente.Email;
                            Session["IDUsuario"] = oBECliente.Id;
                            Session["perfil"] = oBECliente.Perfil;
                        }
                    }
                }
                else
                {
                    BLL.BLLUsuario oBLLLogin = new BLL.BLLUsuario();
                    BEUsuario oBEUsuario = oBLLLogin.Logear(id, email, ServicioEncriptado.Encriptar(contraseña));
                    //Login exitoso
                    if (oBEUsuario != null)
                    {
                        ServicioLogin.Login(oBEUsuario);
                        oBLLLogin.Logear(id, nombreusuario, contraseña);
                        Session["logueado"] = true;
                        Session["statusDB"] = "";
                        Session["perfil"] = oBEUsuario.Perfil;
                        Session["Usuario"] = oBEUsuario.Email;
                        Response.Redirect(@"~\Home.aspx");

                        //Como el login fue exitoso, verifica la integridad de la base de datos
                        if (Session["logueado"] != null)
                        {
                            Session["Usuario"] = oBEUsuario.Email;
                            Session["IDUsuario"] = oBEUsuario.ID;
                            Session["perfil"] = oBEUsuario.Perfil;
                        }
                    }
                    else
                    {
                        string respuesta = "";
                        BEUsuario UsuarioErroneo = new BEUsuario();
                        UsuarioErroneo.Email = email;

                        if (!String.IsNullOrEmpty(respuesta))
                        {
                            //  Literal1.Text = respuesta;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique los datos ingresados');", true);
                        }
                        else
                        {
                            //Literal1.Text = "Usuario o contraseña incorrecto!";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique los datos ingresados');", true);
                        }


                    }
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique los datos ingresados');", true);
                //  Literal1.Text = "El usuario debe tener entre 8 y 50 digitos. La contraseña deben tener entre 8 y 20 digitos.";
            }
        }
                
    }
}