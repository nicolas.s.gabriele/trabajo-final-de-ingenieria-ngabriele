﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BLL;
using BE;
using SERVICIOS;

namespace WebApplication1
{
    public partial class Registracion : System.Web.UI.Page
    {
        BLLCliente oBLLCliente;
        BECliente oBECliente;
        BEUsuario oBEUsuario;
        BLLUsuario oBLLUsuario;
        protected void Page_Load(object sender, EventArgs e)
        {
            oBLLCliente = new BLLCliente();
            oBECliente = new BECliente();
        }

        public void limpiar()
        {
            txtApellido.Text = "";
            txtNombre.Text = "";
            txtDNI.Text = "";
            txtpassword.Text = "";
            txtRepetirPassword.Text = "";
            txtTelefono.Text = "";
            txtEmail.Text = "";

        }
        protected void Button1_Click1(object sender, EventArgs e)
        {
            try
            {
                if (CheckBox1.Checked == true || CheckBox2.Checked == true)
                {
                    if (txtNombre.Text != "" && txtpassword.Text == txtRepetirPassword.Text && CheckBox1.Checked == true)
                    {
                        oBECliente.Nombre = txtNombre.Text;
                        oBECliente.Apellido = txtApellido.Text;
                        oBECliente.Dni = txtDNI.Text;
                        oBECliente.Celular = txtTelefono.Text;
                        oBECliente.Email = txtEmail.Text;
                        oBECliente.Contraseña = ServicioEncriptado.Encriptar(txtpassword.Text);
                        oBLLCliente.Guardar(oBECliente);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('El cliente fue dado de alta con exito');", true);
                        limpiar();
                        Response.Redirect(@"~\Loguin.aspx");
                    }
                    else if (txtNombre.Text != "" && txtpassword.Text == txtRepetirPassword.Text && CheckBox2.Checked == true)
                    {
                        oBEUsuario.Nombre = txtNombre.Text;
                        oBEUsuario.Apellido = txtApellido.Text;
                        oBEUsuario.Contraseña = ServicioEncriptado.Encriptar(txtpassword.Text);
                        oBEUsuario.Email = txtEmail.Text;
                        oBEUsuario.FechaIngreso = DateTime.Now.Date;
                        oBLLUsuario.InsertarUsuario(oBEUsuario);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('El usuario fue dado de alta con exito');", true);
                        limpiar();
                        Response.Redirect(@"~\Loguin.aspx");
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Por favor verifique que haya completado todos los datos');", true);
                }
            }
            catch (Exception)
            {
                throw;
            }
               
        }

        protected void CheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox1.Checked == true)
            {
                CheckBox2.Checked = false;
            }
        }

        protected void CheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox2.Checked == true)
            {
                CheckBox1.Checked = false;
            }
        }
    }
}