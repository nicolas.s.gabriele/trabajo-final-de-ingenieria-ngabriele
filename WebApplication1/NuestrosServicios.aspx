﻿<%@ Page Title="" Language="C#" MasterPageFile="~/PaginasMaestras/Site1.Master" AutoEventWireup="true" CodeBehind="NuestrosServicios.aspx.cs" Inherits="WebApplication1.NuestrosServicios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <link href="CSS/estilo-nuestros-servicios.css" rel="stylesheet" />

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="descripcion-nosotros">

                       <p> Ofrecemos</p>

            <p>•Marketing automation: Permitirá ahorrar tiempo e impulsar el rendimiento al automatizar tu segmentación y mensajes de marketing.</p><br /> 

            <p>•CRM y Segmentación de contactos: Se podrá efectuar la configuración de las relaciones de manera rápida, centralizando, organizando</p>
                <p>y manteniendo dicha información.</p>
                <p>La segmentación mejorara la estrategia de comunicación y se podría transmitir a pequeños grupos con un mensaje a la medida. </p><br />

            <p>•Email marketing: Permitirá impulsar tus servicios y hacer crecer tu negocio con atractivos y profesionales diseños de email.</p><br />

            <p>•Retargeting: Muestra anuncios a los visitantes de tu sitio web mientras navegan por otros sitios y recuérdales por qué eres la mejor solución.</p><br />
        </div>
        </div>
    <div class="col-md-2"></div>
</div>
<br />

</asp:Content>