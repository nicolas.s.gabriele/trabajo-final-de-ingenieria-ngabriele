﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SERVICIOS;
using BE;

namespace SERVICIOS
{
    public class ServicioLogin
    {
        private static ServicioLogin instance;

        private BEUsuario _usuario;

        private BECliente _cliente;

        public BECliente Cliente
        {
            get
            {
                return _cliente;
            }
        }
        public BEUsuario Usuario
        {
            get
            {
                return _usuario;
            }
        }
        private static object _lock = new Object();
        public static void Login(BEUsuario usuario)
        {
            lock (_lock)
            {
                if (instance == null)
                {
                    instance = new ServicioLogin();
                    instance._usuario = usuario;
                }
                else
                {
                    instance._usuario = null;
                    instance = null;
                    //    throw new Exception("La sesion ya ha sido iniciada.");
                }
            }

        }

        public static void LoginCliente(BECliente cliente)
        {
            lock (_lock)
            {
                if (instance == null)
                {
                    instance = new ServicioLogin();
                    instance._cliente = cliente;
                }
                else
                {
                    instance._usuario = null;
                    instance = null;
                    //    throw new Exception("La sesion ya ha sido iniciada.");
                }
            }

        }

        public void Logout()
        {
            if (instance != null)
            {
                _usuario = null;
                instance = null;
            }
        }

        //  private ServicioLogin() { }

        public static ServicioLogin ObtenerSesion
        {
            get
            {
                if (instance == null)
                {
                    instance = new ServicioLogin();
                }
                return instance;
            }
        }
        public bool EstaLogueado()
        {
            return _usuario != null;
        }
    }
}
